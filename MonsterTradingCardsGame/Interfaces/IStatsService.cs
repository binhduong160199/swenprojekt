using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces;

public interface IStatsService
{
    UserStats GetUserStats(string userElo);
}