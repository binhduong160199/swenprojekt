using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IPackageRepository
    {
        void AddPackage(Package package);
        bool CardExists(Guid cardId);
        public int GetNumberOfCardsOwnedByUser(string userId);
        public Package GetAvailablePackage();
        public Card GetCardById(Guid cardId);
        public void AddCardToUser(string userId, Guid cardId);
        public List<CardDTO> GetCardsFromLastPurchasedPackage(string username);
        public bool AreUnacquiredCardsAvailable();
    }
}