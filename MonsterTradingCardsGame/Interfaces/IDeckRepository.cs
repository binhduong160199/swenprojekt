using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IDeckRepository
    {
        public bool ConfigureDeck(string username, List<string> cardIds);
        public List<CardDTO> GetDeckByUsername(string username);
        public UserStats GetUserStatsByUsername(string username);
        public bool DoesDeckConfigurationExist(List<string> cardIdList);
    }
}