using System.Net.Sockets;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IBattleService
    {
        Task<string> EnterLobbyAsync(User user, TcpClient client);
        void WaitsForGame();
        bool IsGameOver();
        Task<string> ExecuteRoundAsync(string username1, string username2);

        string HandleBattleOutcome(Card team1Card, Card team2Card, string team1Result, string team2Result,
            string username1, string username2);

        string DetermineOutcome(string username1, string username2);
        public void RemoveLoseCardFromDeck(Card card);
        int CountCardInDeck(string username);
    }
}