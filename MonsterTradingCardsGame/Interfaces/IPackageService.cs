using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IPackageService
    {
        bool CreatePackage(Package package, out string message);
        bool HasEnoughMoney(string userId);
        public bool IsPackageAvailable(string userId);
        public bool ProcessPurchase(string userId);
        public void RecordPackageAcquisition(string userId);
        public List<CardDTO> GetCardsFromLastPurchasedPackage(string username);
    }
}