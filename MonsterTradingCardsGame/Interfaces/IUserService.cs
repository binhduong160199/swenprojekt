using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IUserService
    {
        public bool RegisterUser(string username, string password);
        public UserCredentials RetrieveUserData(string username);
        public bool UpdateUserProfile(string username, UserData updatedUserData);
        public bool AuthenticateUser(string username, string password);
        public UserData RetrieveUserDataInUsers(string username);
        public bool DoesUserExist(string username);
    }
}