using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface ICardRepository
    {
        List<CardDTO> GetCardsByUsername(string username);
        public List<CardDTO> GetCardsByUsernameForBattle(string username);
    }
}