using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface ICardService
    {
        List<CardDTO> GetCardsByUsername(string username);
    }
}