using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces;

public interface IScoreboardRepository
{
    List<UserStats> GetUserScoreboardByUserElo();
}