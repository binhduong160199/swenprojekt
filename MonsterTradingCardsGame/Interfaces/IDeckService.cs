using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IDeckService
    {
        public bool ConfigureDeck(string username, List<string> cardIds);
        List<CardDTO> GetDeckByUsername(string username);
        public User GetUserAndCardInDeck(string username);
        public bool IsDeckAlreadyConfigured(List<string> cardIdList);
    }
}