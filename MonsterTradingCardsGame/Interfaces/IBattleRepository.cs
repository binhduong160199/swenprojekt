using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IBattleRepository
    {
        public void ChangeStatsAfterBattle(string userWin, string userLose);
        public void RemoveLoseCardFromDeck(Card card);
        int CountCardInDeck(string username);
    }
}