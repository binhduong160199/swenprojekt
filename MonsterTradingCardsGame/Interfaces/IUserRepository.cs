using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface IUserRepository
    {
        public bool TestDatabaseConnection();
        public void CreateUser(UserCredentials user);
        public UserCredentials GetUserByUsername(string username);
        public bool UpdateUserProfile(string username, UserData updatedUserData);
        public string GetPasswordHashByUsername(string username);
        public UserData? GetUserDataByUsername(string username);
        public bool DoesUserExist(string username);
    }
}