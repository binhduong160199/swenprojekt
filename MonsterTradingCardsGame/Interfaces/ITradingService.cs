using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces
{
    public interface ITradingService
    {
        public List<TradingDeal> GetTradingDeal(string username);
        bool CheckCardIfUserNotOwnOrInDeck(TradingDeal tradingDeal, string username);
        bool CheckDealExist(TradingDeal tradingDeal, string username);
        public void CreateTrading(TradingDeal tradingDeal);
        bool UserHasDeal(Guid tradingId, string username);
        bool FindDeal(Guid tradingId);
        public void DeleteTrading(Guid tradingId);
        bool IfCardIsGoodEnoughOrNotYourOwnCard(Guid tradingId, string username);
        public void DoTrading(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete);
        public Guid FindTheCardOfTradingId(Guid tradingId);
        public bool UserTryToTradeToHimSelf(Guid tradingId, string username);
    }
}