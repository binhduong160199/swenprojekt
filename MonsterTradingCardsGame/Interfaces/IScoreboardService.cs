using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces;

public interface IScoreboardService
{
    List<UserStats> GetUserScoreboard();
}