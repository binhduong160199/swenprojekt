using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Interfaces;

public interface IStatsRepository
{
    UserStats GetUserStatsByUsername(string username);
}