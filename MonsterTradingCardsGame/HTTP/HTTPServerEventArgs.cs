﻿using System.Net.Sockets;
using System.Text;

namespace MonsterTradingCardsGame.HTTP
{
    /// <summary>
    /// This class provides HTTP server event arguments
    /// </summary>
    public class HttpSvrEventArgs : EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>TCP client.</summary>
        public TcpClient Client;


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="client">TCP client object.</param>
        /// <param name="plainMessage">HTTP plain message.</param>
        public HttpSvrEventArgs(TcpClient client, string plainMessage)
        {
            Client = client;
            PlainMessage = plainMessage;
            Payload = string.Empty;

            string[] lines = plainMessage.Replace("\r\n", "\n").Replace("\r", "\n").Split('\n');
            bool inheaders = true;
            List<HttpHeader> headers = new();

            for (int i = 0; i < lines.Length; i++)
            {
                if (i == 0)
                {
                    string[] inc = lines[0].Split(' ');
                    Method = inc[0];
                    Path = inc[1];
                }
                else if (inheaders)
                {
                    if (string.IsNullOrWhiteSpace(lines[i]))
                    {
                        inheaders = false;
                    }
                    else
                    {
                        headers.Add(new HttpHeader(lines[i]));
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(Payload))
                    {
                        Payload += "\r\n";
                    }

                    Payload += lines[i];
                }
            }

            Headers = headers.ToArray();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the plain HTTP message.</summary>
        public string PlainMessage { get; protected set; }


        /// <summary>Gets the HTTP method.</summary>
        public virtual string Method { get; protected set; } = string.Empty;


        /// <summary>Gets the request path.</summary>
        public virtual string Path { get; protected set; } = string.Empty;


        /// <summary>Gets the HTTP hgeaders.</summary>
        public virtual HttpHeader[] Headers { get; protected set; }


        /// <summary>Gets the HTTP payload.</summary>
        public virtual string Payload { get; protected set; }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a reply to the HTTP request.</summary>
        /// <param name="status">Status code.</param>
        /// <param name="payload">Payload.</param>
        public virtual void Reply(int status, string? payload = null)
        {
            try
            {
                if (Client != null && Client.Connected)
                {
                    string data = $"HTTP/1.1 {status} {GetStatusDescription(status)}\n";
                    data += "Content-Type: text/plain\n";

                    if (!string.IsNullOrEmpty(payload))
                    {
                        data += $"Content-Length: {Encoding.ASCII.GetByteCount(payload)}\n\n";
                        data += payload;
                    }
                    else
                    {
                        data += "Content-Length: 0\n\n";
                    }

                    byte[] buf = Encoding.ASCII.GetBytes(data);
                    Client.GetStream().Write(buf, 0, buf.Length);
                }
            }
            catch (ObjectDisposedException)
            {
                // Log the error or handle it as needed
                Console.WriteLine("TcpClient has been disposed.");
            }
            finally
            {
                Client?.Close();
                Client?.Dispose();
            }
        }

        private string GetStatusDescription(int status)
        {
            switch (status)
            {
                case 200: return "OK";
                case 201: return "Created";
                case 204: return "No Content";
                case 400: return "Bad Request";
                case 401: return "Unauthorized";
                case 403: return "Forbidden";
                case 404: return "Not Found";
                case 409: return "Conflict";
                case 500: return "Internal Server Error";
                default: return "I'm a Teapot"; // or some other default message
            }
        }
    }
}