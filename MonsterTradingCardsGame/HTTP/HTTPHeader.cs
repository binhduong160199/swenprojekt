﻿namespace MonsterTradingCardsGame.HTTP
{
    /// <summary>
    /// This class represents HTTP headers.
    /// </summary>
    public class HttpHeader
    {
        private Dictionary<string, string> _headers = new Dictionary<string, string>();

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="header">Header text.</param>
        public HttpHeader(string header)
        {
            try
            {
                int n = header.IndexOf(':');
                if (n != -1)
                {
                    string name = header.Substring(0, n).Trim();
                    string value = header.Substring(n + 1).Trim();

                    _headers[name] = value;
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>Gets the header value by name.</summary>
        /// <param name="name">The name of the header.</param>
        /// <returns>The header value or null if not found.</returns>
        public string this[string name]
        {
            get { return _headers.ContainsKey(name) ? _headers[name] : null; }
        }

        /// <summary>Checks if a header with the specified name exists.</summary>
        /// <param name="name">The name of the header.</param>
        /// <returns>True if the header exists; otherwise, false.</returns>
        public bool ContainsKey(string name)
        {
            return _headers.ContainsKey(name);
        }

        // Add other necessary methods or properties
    }
}