﻿namespace MonsterTradingCardsGame.Models
{
    public class TradingDeal
    {
        public Guid ID { get; set; }
        public Guid CardToTrade { get; set; }
        public string Type { get; set; }
        public double MinimumDamage { get; set; }
    }
}