﻿namespace MonsterTradingCardsGame.Models
{
    public class UserData
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Image { get; set; }

        public UserData(string username, string name, string bio, string image)
        {
            Username = username;
            Name = name;
            Bio = bio;
            Image = image;
        }
    }
}