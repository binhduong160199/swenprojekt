namespace MonsterTradingCardsGame.Models;

public class User
{
    public string Username { get; }

    public CardDTO[] Deck { get; } = new CardDTO[4];

    public UserStats UserStats { get; set; } = new UserStats();

    public User(string username, CardDTO[] deck, UserStats stats)
    {
        Username = username;
        Deck = deck;
        UserStats = stats;
    }

    public void UpdateStats(bool won)
    {
        if (won)
        {
            UserStats.Wins++;
            UserStats.Elo += 3;
        }
        else
        {
            UserStats.Losses++;
            UserStats.Elo -= 5;
        }
    }
}