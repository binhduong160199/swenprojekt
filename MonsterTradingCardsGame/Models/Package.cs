namespace MonsterTradingCardsGame.Models
{
    public class Package
    {
        public Guid Id { get; set; }
        public List<Card> Cards { get; set; }

        // Default constructor for creating new packages
        public Package()
        {
            Id = Guid.NewGuid();
            Cards = new List<Card>();
        }

        // Constructor for existing packages (e.g., from database)
        public Package(Guid id, List<Card> cards)
        {
            Id = id;
            Cards = cards;
        }

        public Package(List<Card> cards) : this()
        {
            Cards = cards;
        }
    }
}