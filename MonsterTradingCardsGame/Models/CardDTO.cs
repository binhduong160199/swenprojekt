namespace MonsterTradingCardsGame.Models
{
    public class CardDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Damage { get; set; }

        public Card CreateCard(Guid Id, string Name, double Damage)
        {
            CardTypeEnum cardType = DetermineCardType(Name);

            switch (cardType)
            {
                case CardTypeEnum.Spell:
                    return new SpellCard(Id, Name, Damage);

                case CardTypeEnum.Monster:
                default:
                    return new MonsterCard(Id, Name, Damage);
            }
        }

        public Card CreateCard()
        {
            CardTypeEnum cardType = DetermineCardType(Name);

            switch (cardType)
            {
                case CardTypeEnum.Spell:
                    return new SpellCard(Id, Name, Damage);

                case CardTypeEnum.Monster:
                default:
                    return new MonsterCard(Id, Name, Damage);
            }
        }

        public CardTypeEnum DetermineCardType(string name)
        {
            if (name.Contains("Spell"))
            {
                return CardTypeEnum.Spell;
            }
            else
            {
                return CardTypeEnum.Monster;
            }
        }
    }

    public enum CardTypeEnum
    {
        Monster,
        Spell
    }
}