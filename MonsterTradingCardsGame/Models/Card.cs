﻿namespace MonsterTradingCardsGame.Models
{
    public abstract class Card
    {
        public Guid Id { get; private set; }
        public string Name { get; set; }
        public double Damage { get; set; }

        protected Card(Guid id, string name, double damage)
        {
            Id = id;
            Name = name;
            Damage = damage;
        }

        public virtual (string gameOutcome, double Damage, double opponentDamage) Attack(Card? opponent)
        {
            return ("", 0, 0);
        }
    }
}