namespace MonsterTradingCardsGame.Models
{
    public class PackageDTO
    {
        public List<CardDTO> Cards { get; set; }
    }
}