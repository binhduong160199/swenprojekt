﻿namespace MonsterTradingCardsGame.Models
{
    public class MonsterCard : Card
    {
        public MonsterCard(Guid id, string name, double damage)
            : base(id, name, damage)
        {
        }

        public override (string gameOutcome, double Damage, double opponentDamage) Attack(Card? opponent)
        {
            if (Name.Contains("Goblin") & opponent.Name.Contains("Dragon"))
            {
                Console.WriteLine("Goblins are too afraid of Dragons to attack");
                return ("lose", Damage, opponent.Damage);
            }

            if (Name.Contains("Wizzard") & opponent.Name.Contains("Ork"))
            {
                Console.WriteLine("Wizzard can control Orks so they are not able to damage them");
                return ("win", Damage, opponent.Damage);
            }

            if (Name.Contains("Knight") & opponent.Name.Contains("WaterSpell"))
            {
                Console.WriteLine("The armor of Knights is so heavy that WaterSpells make them drown them instantly");
                return ("lose", Damage, opponent.Damage);
            }

            if (Name.Contains("Kraken") & opponent.Name.Contains("Spell"))
            {
                Console.WriteLine("The Kraken is immune against spells");
                return ("win", Damage, opponent.Damage);
            }

            if (Name.Contains("FireElves") & opponent.Name.Contains("Dragon"))
            {
                Console.WriteLine("The FireElves know Dragons since they were little and can evade their attacks");
                return ("draw", Damage, opponent.Damage);
            }

            string gameOutcome = Damage > opponent.Damage ? "win" : Damage < opponent.Damage ? "lose" : "draw";
            return (gameOutcome, Damage, opponent.Damage);
        }
    }
}