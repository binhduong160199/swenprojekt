﻿namespace MonsterTradingCardsGame.Models
{
    public class SpellCard : Card
    {
        public SpellCard(Guid id, string name, double damage)
            : base(id, name, damage) // Call the base constructor without CardType
        {
        }

        public override (string gameOutcome, double Damage, double opponentDamage) Attack(Card? opponent)
        {
            if (Name.Contains("Spell") & opponent.Name.Contains("Kraken"))
            {
                return ("lose", Damage, opponent.Damage); // Spells do not affect Kraken
            }

            // For Fire Spell
            if (Name.Contains("FireSpell") & opponent.Name.Contains("WaterSpell"))
            {
                return ("lose", Damage / 2, opponent.Damage * 2);
            }

            if (Name.Contains("FireSpell") & opponent.Name.Contains("RegularSpell"))
            {
                return ("win", Damage * 2, opponent.Damage / 2);
            }

            // For Water Spell
            if (Name.Contains("WaterSpell") & opponent.Name.Contains("FireSpell"))
            {
                return ("win", Damage * 2, opponent.Damage / 2);
            }

            if (Name.Contains("WaterSpell") & opponent.Name.Contains("RegularSpell"))
            {
                return ("win", Damage / 2, opponent.Damage * 2);
            }

            // For Regular Spell
            if (Name.Contains("RegularSpell") & opponent.Name.Contains("WaterSpell"))
            {
                return ("win", Damage * 2, opponent.Damage / 2);
            }

            if (Name.Contains("RegularSpell") & opponent.Name.Contains("FireSpell"))
            {
                return ("win", Damage / 2, opponent.Damage * 2);
            }

            string gameOutcome = Damage > opponent.Damage ? "win" : Damage < opponent.Damage ? "lose" : "draw";
            return (gameOutcome, Damage, opponent.Damage);
        }
    }
}