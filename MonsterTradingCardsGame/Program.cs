using MonsterTradingCardsGame.Services;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.HTTP;
using MonsterTradingCardsGame.Interfaces;

namespace MonsterTradingCardsGame
{
    class Program
    {
        private static IUserService _userService;
        private static IPackageService _packageService;
        private static ICardService _cardService;
        private static IDeckService _deckService;
        private static IBattleService _battleService;
        private static IStatsService _statsService;
        private static IScoreboardService _scoreboardService;
        private static ITradingService _tradingService;

        static void Main(string[] args)
        {
            //Connection
            var dbConnection = new DatabaseConnection();

            //User
            IUserRepository userRepository = new UserRepository(dbConnection);
            _userService = new UserService(userRepository);

            //Package
            IPackageRepository packageRepository = new PackageRepository(dbConnection);
            _packageService = new PackageService(packageRepository, userRepository);

            // Card
            ICardRepository cardRepository = new CardRepository(dbConnection);
            _cardService = new CardService(cardRepository);

            // Deck
            IDeckRepository deckRepository = new DeckRepository(dbConnection);
            _deckService = new DeckService(deckRepository, cardRepository, packageRepository);

            //Battle
            IBattleRepository battleRepository = new BattleRepository(dbConnection);
            _battleService = new BattleService(battleRepository);

            //Stats
            IStatsRepository statsRepository = new StatsRepository(dbConnection);
            _statsService = new StatsService(statsRepository);

            //Scoreboard
            IScoreboardRepository scoreboardRepository = new ScoreboardRepository(dbConnection);
            _scoreboardService = new ScoreboardService(scoreboardRepository);

            //Trading
            ITradingRepository tradingRepository = new TradingRepository(dbConnection);
            _tradingService = new TradingService(tradingRepository);

            // Test database connection
            if (userRepository.TestDatabaseConnection())
            {
                Console.WriteLine("Database connection successful.");
            }
            else
            {
                Console.WriteLine("Database connection failed.");
                return; // Exit if the database connection fails
            }

            // Instantiate UserController with the UserService
            Controller.UserController userController = new Controller.UserController(_userService);

            // Instantiate PackageController with the PackageService and UserService
            Controller.PackageController packageController =
                new Controller.PackageController(_packageService, _userService);

            // Instantiate CardController with the CardService and UserService
            Controller.CardController cardController = new Controller.CardController(_cardService, _userService);

            // Instantiate DeckController with the DeckService and UserService
            Controller.DeckController deckController = new Controller.DeckController(_deckService, _userService);

            // Instantiate BattleController with necessary services
            Controller.BattleController battleController =
                new Controller.BattleController(_battleService, _deckService, _cardService);

            // Instantiate StatsController with necessary services
            Controller.StatsController statsController = new Controller.StatsController(_statsService, _userService);

            // Instantiate ScoreboardController with necessary services
            Controller.ScoreboardController scoreboardController =
                new Controller.ScoreboardController(_scoreboardService, _userService);

            // Instantiate ScoreboardController with necessary services
            Controller.TradingController tradingController =
                new Controller.TradingController(_tradingService, _userService);

            // HTTP part
            HttpSvr svr = new();

            svr.Incoming += (_, e) =>
            {
                if (e.Path.StartsWith("/users") || e.Path.Equals("/users") || e.Path.Equals("/sessions"))
                {
                    userController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/packages") || e.Path.Equals("/packages") ||
                         e.Path.StartsWith("/transactions"))
                {
                    packageController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/cards"))
                {
                    cardController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/deck"))
                {
                    deckController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/stats"))
                {
                    statsController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/scoreboard"))
                {
                    scoreboardController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/tradings"))
                {
                    tradingController.ProcessMessage(e);
                }
                else if (e.Path.StartsWith("/battles"))
                {
                    battleController.ProcessMessage(e);
                }
                else
                {
                    e.Reply(404, "Not Found");
                }
            };

            // Start the HTTP server in a separate thread

            Thread serverThread = new Thread(() => svr.Run());
            serverThread.Start();

            Console.WriteLine("Server started. Press Enter to stop."); // Enter to stop the server
            Console.ReadLine();
            Console.WriteLine("Stopping server...");
            svr.Stop();
            serverThread.Join(); // Wait for the server thread to finish
            Console.WriteLine("Server stopped.");
        }
    }
}