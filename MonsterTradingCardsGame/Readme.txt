# Monster Trading Card Game

Welcome to the repository for the Monster Trading Card Game, developed as part of the SWEN 1 course at FH Technikum Wien.

## Overview

The Monster Trading Card Game is a digital card game that simulates the classic face-to-face trading card games. Players can collect cards, build decks, and engage in battles with others. This project outlines the rules and conventions for communication between the client and server, detailing the player actions, network transmission, and server responses.

## Features

- **User Authentication**: Secure login and registration system for players.
- **Deck Management**: Players can create and customize their decks.
- **Trading System**: A robust trading system allows players to exchange cards.
- **Battle Mechanics**: Engage in strategic battles with other players.
- **Leaderboards**: Track your progress and compete with the top players.

## Getting Started

To get started with the Monster Trading Card Game, clone this repository and follow the setup instructions below:
git clone : https://gitlab.com/binhduong160199/swenprojekt

Thank you for your interest in the Monster Trading Card Game!

