using MonsterTradingCardsGame.HTTP;
using MonsterTradingCardsGame.Interfaces;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace MonsterTradingCardsGame.Controller
{
    public class BattleController
    {
        private readonly IBattleService _battleService;
        private readonly IDeckService _deckService;
        private readonly ICardService _cardService;

        public BattleController(IBattleService battleService, IDeckService deckService, ICardService cardService)
        {
            _battleService = battleService;
            _deckService = deckService;
            _cardService = cardService;
        }

        public async Task ProcessMessage(HttpSvrEventArgs e)
        {
            if (!IsAuthenticated(e))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var username = GetUsernameFromToken(GetTokenFromHeaders(e.Headers));
            try
            {
                if (e.Method == "POST")
                {
                    HandleBattlesRequest(e);
                }
            }
            catch (Exception exception)
            {
                e.Reply(404, "Not Found");
            }
        }

        private async Task HandleBattlesRequest(HttpSvrEventArgs e)
        {
            try
            {
                var user = _deckService.GetUserAndCardInDeck(GetUsernameFromToken(GetTokenFromHeaders(e.Headers)));

                if (user == null)
                {
                    e.Reply(500, "Error to get user stats and card in deck");
                }

                // Start the game in a new thread and pass the client
                if (user != null)
                {
                    string gameResult = await _battleService.EnterLobbyAsync(user, e.Client);
                    e.Reply(200, gameResult); // Reply with the game result
                }
                else
                {
                    e.Reply(500, "Error to get user stats and card in deck");
                }
            }
            catch (Exception ex)
            {
                e.Reply(500, "Internal Server Error: " + ex.Message);
            }
        }

        private bool IsAuthenticated(HttpSvrEventArgs e)
        {
            var authHeader = e.Headers.FirstOrDefault(h => h["Authorization"] != null);
            var token = authHeader?["Authorization"];

            if (string.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
            {
                return false;
            }

            // Extract the JWT token
            var jwtToken = token.Substring("Bearer ".Length).Trim();
            // Validate the token
            return ValidateJwtToken(jwtToken);
        }

        private bool ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false, // Validate other aspects of the token as needed, e.g., lifetime
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero // reduce or eliminate clock skew tolerance
                }, out SecurityToken validatedToken);
                // The token is valid
                return true;
            }
            catch
            {
                // Token is not valid
                return false;
            }
        }

        private string GetUsernameFromToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                Console.WriteLine("Token is null or whitespace");
                throw new ArgumentException("Token is null or whitespace", nameof(token));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            if (jwtToken == null)
            {
                Console.WriteLine("JWT Token is null");
                return null;
            }

            // Check for the "unique_name" claim which holds the username in token.
            var usernameClaim = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "unique_name");
            return usernameClaim?.Value;
        }

        private string GetTokenFromHeaders(HttpHeader[] headers)
        {
            foreach (var header in headers)
            {
                if (header.ContainsKey("Authorization"))
                {
                    var token = header["Authorization"];
                    if (!string.IsNullOrEmpty(token) && token.StartsWith("Bearer "))
                    {
                        return token.Substring("Bearer ".Length).Trim();
                    }
                }
            }

            return null; // Return null if no token is found
        }
    }
}