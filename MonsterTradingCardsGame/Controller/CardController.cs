using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using MonsterTradingCardsGame.HTTP;
using MonsterTradingCardsGame.Interfaces;
using Newtonsoft.Json;

namespace MonsterTradingCardsGame.Controller
{
    public class CardController
    {
        private readonly ICardService _cardService;
        private readonly IUserService _userService;

        public CardController(ICardService cardService, IUserService userService)
        {
            _cardService = cardService;
            _userService = userService;
        }

        public void ProcessMessage(HttpSvrEventArgs e)
        {
            try
            {
                if (e.Method == "GET" && e.Path == "/cards")
                {
                    GetCardsForUser(e);
                }
                else
                {
                    e.Reply(404, "Not Found");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error processing message: {ex.Message}");
                e.Reply(500, "Internal Server Error");
            }
        }

        private void GetCardsForUser(HttpSvrEventArgs e)
        {
            if (!IsAuthenticated(e))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var token = GetTokenFromHeaders(e.Headers);
            var username = GetUsernameFromToken(token);
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var cards = _cardService.GetCardsByUsername(username);
            if (cards != null && cards.Count > 0)
            {
                var jsonResponse = JsonConvert.SerializeObject(cards);
                e.Reply(200, jsonResponse);
            }
            else if (cards != null)
            {
                e.Reply(204, "The request was fine, but the user doesn't have any cards");
            }
            else
            {
                e.Reply(500, "Internal Server Error");
            }
        }

        private bool IsAuthenticated(HttpSvrEventArgs e)
        {
            var authHeader = e.Headers.FirstOrDefault(h => h["Authorization"] != null);
            var token = authHeader?["Authorization"];

            if (string.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
            {
                return false;
            }

            // Extract the JWT token
            var jwtToken = token.Substring("Bearer ".Length).Trim();
            // Validate the token
            return ValidateJwtToken(jwtToken);
        }

        private bool ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false, // Validate other aspects of the token as needed, e.g., lifetime
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero // reduce or eliminate clock skew tolerance
                }, out SecurityToken validatedToken);
                // The token is valid
                return true;
            }
            catch
            {
                // Token is not valid
                return false;
            }
        }

        private string GetUsernameFromToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                Console.WriteLine("Token is null or whitespace");
                throw new ArgumentException("Token is null or whitespace", nameof(token));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            if (jwtToken == null)
            {
                Console.WriteLine("JWT Token is null");
                return null;
            }

            // Check for the "unique_name" claim which holds the username in token.
            var usernameClaim = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "unique_name");
            return usernameClaim?.Value;
        }

        private string GetTokenFromHeaders(HttpHeader[] headers)
        {
            foreach (var header in headers)
            {
                if (header.ContainsKey("Authorization"))
                {
                    var token = header["Authorization"];
                    if (!string.IsNullOrEmpty(token) && token.StartsWith("Bearer "))
                    {
                        return token.Substring("Bearer ".Length).Trim();
                    }
                }
            }

            return null; // Return null if no token is found
        }
    }
}