using MonsterTradingCardsGame.HTTP;
using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;
using Newtonsoft.Json;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace MonsterTradingCardsGame.Controller
{
    public class TradingController
    {
        private readonly ITradingService _tradingService;
        private readonly IUserService _userService;

        public TradingController(ITradingService tradingService, IUserService userService)
        {
            _tradingService = tradingService;
            _userService = userService;
        }

        public void ProcessMessage(HttpSvrEventArgs e)
        {
            if (e.Method == "GET")
            {
                HandleGetTrading(e);
            }
            else if (e.Method == "POST" && e.Path == "/tradings")
            {
                HandleCreateTrading(e);
            }
            else if (e.Method == "POST" && e.Path.StartsWith("/tradings/"))
            {
                HandleTrading(e);
            }
            else if (e.Method == "DELETE")
            {
                HandleDeleteTrading(e);
            }
            else
            {
                e.Reply(404, "Not Found");
            }
        }

        private void HandleTrading(HttpSvrEventArgs e)
        {
            try
            {
                string[] pathSegments = e.Path.Split('/');
                if (pathSegments.Length < 3 || !Guid.TryParse(pathSegments[2], out Guid tradingDealId))
                {
                    e.Reply(400, "Invalid trading deal ID"); // Bad Request
                    return;
                }

                // the card want to be traded
                Guid tradingIdWantTrade = new Guid(pathSegments[2]);
                Guid idOfTheCardWantToTrade = _tradingService.FindTheCardOfTradingId(tradingIdWantTrade);
                // the card the user offers
                var tradingCard = JsonConvert.DeserializeObject<TradingCard>(e.Payload);
                Guid tradingCardIdHas = tradingCard.Id;
                
                if (!IsAuthenticated(e))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var token = GetTokenFromHeaders(e.Headers);
                var username = GetUsernameFromToken(token);
                if (string.IsNullOrEmpty(username))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                if (!_tradingService.FindDeal(tradingIdWantTrade))
                {
                    e.Reply(404, "The provided deal ID was not found");
                    return;
                }

                if (!_tradingService.IfCardIsGoodEnoughOrNotYourOwnCard(tradingIdWantTrade, username))
                {
                    e.Reply(403,
                        "The offered card is not owned by the user, or the requirements are not met (Type, MinimumDamage), or the offered card is locked in the deck.");
                    return;
                }

                // prevent not to trade with your self
                if (_tradingService.UserTryToTradeToHimSelf(tradingIdWantTrade, username))
                {
                    e.Reply(403,
                        "The offered card is not owned by the user, or the requirements are not met (Type, MinimumDamage), or the offered card is locked in the deck.");
                    return;
                }

                // card id
                _tradingService.DoTrading(tradingCardIdHas, idOfTheCardWantToTrade, tradingIdWantTrade);

                e.Reply(200, "Trading deal successfully executed"); // OK
            }
            catch (Exception ex)
            {
                e.Reply(500, "Internal Server Error: " + ex.Message); // Internal Server Error
            }
        }

        private void HandleDeleteTrading(HttpSvrEventArgs e)
        {
            try
            {
                // Extract the tradingdealid from the URL
                string[] pathSegments = e.Path.Split('/');
                if (pathSegments.Length < 3 || !Guid.TryParse(pathSegments[2], out Guid tradingDealId))
                {
                    e.Reply(400, "Invalid trading deal ID");
                    return;
                }

                Guid tradingId = new Guid(pathSegments[2]);

                if (!IsAuthenticated(e))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var token = GetTokenFromHeaders(e.Headers);
                var username = GetUsernameFromToken(token);
                if (string.IsNullOrEmpty(username))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                if (!_tradingService.UserHasDeal(tradingId, username))
                {
                    e.Reply(403, "The deal contains a card that is not owned by the user.");
                    return;
                }

                if (!_tradingService.FindDeal(tradingId))
                {
                    e.Reply(404, "The provided deal ID was not found");
                    return;
                }

                _tradingService.DeleteTrading(tradingId);
                e.Reply(200, "Trading deal deleted successfully");
            }
            catch (Exception ex)
            {
                e.Reply(500, "Internal Server Error: " + ex.Message);
            }
        }

        private void HandleGetTrading(HttpSvrEventArgs e)
        {
            try
            {
                if (!IsAuthenticated(e))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var token = GetTokenFromHeaders(e.Headers);
                var username = GetUsernameFromToken(token);
                if (string.IsNullOrEmpty(username))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var getTradingDeal = _tradingService.GetTradingDeal(username);
                var json = JsonConvert.SerializeObject(getTradingDeal);

                if (String.IsNullOrEmpty(json) || json == "[]")
                {
                    e.Reply(204);
                }
                else
                {
                    e.Reply(200, json);
                }
            }
            catch (Exception ex)
            {
                e.Reply(500, "Internal Server Error: " + ex.Message);
            }
        }

        private void HandleCreateTrading(HttpSvrEventArgs e)
        {
            try
            {
                if (!IsAuthenticated(e))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var token = GetTokenFromHeaders(e.Headers);
                var username = GetUsernameFromToken(token);
                if (string.IsNullOrEmpty(username))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                TradingDeal tradingDeal = JsonConvert.DeserializeObject<TradingDeal>(e.Payload);

                if (tradingDeal == null)
                {
                    e.Reply(400, "no trading information");
                    return;
                }

                if (!_tradingService.CheckCardIfUserNotOwnOrInDeck(tradingDeal, username))
                {
                    e.Reply(403, "The deal contains a card that is not owned by the user or locked in the deck.");
                    return;
                }

                if (_tradingService.CheckDealExist(tradingDeal, username))
                {
                    e.Reply(409, "A deal with this deal ID already exists");
                    return;
                }

                _tradingService.CreateTrading(tradingDeal);
                e.Reply(201, "Trading deal successfully created");
            }
            catch (Exception ex)
            {
                e.Reply(500, "Internal Server Error: " + ex.Message);
            }
        }

        private bool IsAuthenticated(HttpSvrEventArgs e)
        {
            var authHeader = e.Headers.FirstOrDefault(h => h["Authorization"] != null);
            var token = authHeader?["Authorization"];

            if (string.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
            {
                return false;
            }

            // Extract the JWT token
            var jwtToken = token.Substring("Bearer ".Length).Trim();
            // Validate the token
            return ValidateJwtToken(jwtToken);
        }

        private bool ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false, // Validate other aspects of the token as needed, e.g., lifetime
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero // reduce or eliminate clock skew tolerance
                }, out SecurityToken validatedToken);
                // The token is valid
                return true;
            }
            catch
            {
                // Token is not valid
                return false;
            }
        }

        private string GetUsernameFromToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                Console.WriteLine("Token is null or whitespace");
                throw new ArgumentException("Token is null or whitespace", nameof(token));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            if (jwtToken == null)
            {
                Console.WriteLine("JWT Token is null");
                return null;
            }

            // Check for the "unique_name" claim which holds the username in token.
            var usernameClaim = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "unique_name");
            return usernameClaim?.Value;
        }

        private string GetTokenFromHeaders(HttpHeader[] headers)
        {
            foreach (var header in headers)
            {
                if (header.ContainsKey("Authorization"))
                {
                    var token = header["Authorization"];
                    if (!string.IsNullOrEmpty(token) && token.StartsWith("Bearer "))
                    {
                        return token.Substring("Bearer ".Length).Trim();
                    }
                }
            }

            return null; // Return null if no token is found
        }
    }
}