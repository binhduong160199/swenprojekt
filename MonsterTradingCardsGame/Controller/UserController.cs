using MonsterTradingCardsGame.HTTP;
using Newtonsoft.Json;
using MonsterTradingCardsGame.Models;
using System.IdentityModel.Tokens.Jwt;
using MonsterTradingCardsGame.Interfaces;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace MonsterTradingCardsGame.Controller
{
    public class UserController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public void ProcessMessage(HttpSvrEventArgs e)
        {
            switch (e.Method)
            {
                case "POST":
                    if (e.Path == "/users")
                        RegisterUser(e);
                    else if (e.Path.StartsWith("/sessions"))
                        LoginUser(e);
                    break;
                case "GET":
                    if (e.Path.StartsWith("/users/"))
                        RetrieveUser(e);
                    break;
                case "PUT":
                    if (e.Path.StartsWith("/users/"))
                        UpdateUserProfile(e);
                    break;
                default:
                    e.Reply(405, "Method Not Allowed");
                    break;
            }
        }

        private void RegisterUser(HttpSvrEventArgs e)
        {
            try
            {
                //convert the JSON data in to an object of the UserCredentials
                var userCredentials = JsonConvert.DeserializeObject<UserCredentials>(e.Payload);
                if (userCredentials == null || string.IsNullOrWhiteSpace(userCredentials.Username) ||
                    string.IsNullOrWhiteSpace(userCredentials.Password))
                {
                    e.Reply(400, "Invalid request");
                    return;
                }

                bool userCreated = _userService.RegisterUser(userCredentials.Username, userCredentials.Password);
                if (userCreated)
                {
                    e.Reply(201, "User successfully created");
                }
                else
                {
                    e.Reply(409, "User with same username already registered");
                }
            }
            catch (Exception)
            {
                e.Reply(500, "Internal server error");
            }
        }

        private void RetrieveUser(HttpSvrEventArgs e)
        {
            try
            {
                if (!IsAuthenticated(e))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var token = GetTokenFromHeaders(e.Headers);
                var username = GetUsernameFromToken(token);
                if (string.IsNullOrEmpty(username))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var urlUsername = GetUsernameFromUrl(e);
                bool checkUsername = _userService.DoesUserExist(urlUsername);
                if (checkUsername)
                {
                    if (urlUsername != username)
                    {
                        e.Reply(403,
                            "Access denied"); // 403 Forbidden if the user is trying to update another user's profile
                        return;
                    }

                    var userData = _userService.RetrieveUserDataInUsers(username);
                    if (userData != null)
                    {
                        string jsonResponse = JsonConvert.SerializeObject(userData);
                        e.Reply(200, jsonResponse); // OK: Data successfully retrieved
                    }
                }
                else
                {
                    e.Reply(404, "User not found");
                }
            }
            catch (Exception)
            {
                e.Reply(401, "Access token is missing or invalid");
            }
        }

        private void LoginUser(HttpSvrEventArgs e)
        {
            try
            {
                var loginCredentials = JsonConvert.DeserializeObject<UserCredentials>(e.Payload);
                if (loginCredentials == null || string.IsNullOrWhiteSpace(loginCredentials.Username) ||
                    string.IsNullOrWhiteSpace(loginCredentials.Password))
                {
                    e.Reply(400, "Invalid request");
                    return;
                }

                bool isAuthenticated =
                    _userService.AuthenticateUser(loginCredentials.Username, loginCredentials.Password);
                if (isAuthenticated)
                {
                    string token = GenerateJwtToken(loginCredentials.Username);
                    e.Reply(200, JsonConvert.SerializeObject(new { Token = token }));
                }
                else
                {
                    e.Reply(401, "Unauthorized: Incorrect username or password");
                }
            }
            catch (Exception)
            {
                e.Reply(500, "Internal server error");
            }
        }

        private string GenerateJwtToken(string username)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, username)
                    }),
                    Expires = DateTime.UtcNow.AddHours(2),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                        SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error generating JWT token: {ex.Message}");
                if (ex.InnerException != null)
                {
                    Console.WriteLine($"Inner exception: {ex.InnerException.Message}");
                }

                return null;
            }
        }

        private void UpdateUserProfile(HttpSvrEventArgs e)
        {
            try
            {
                if (!IsAuthenticated(e))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var token = GetTokenFromHeaders(e.Headers);
                var username = GetUsernameFromToken(token);
                if (string.IsNullOrEmpty(username))
                {
                    e.Reply(401, "Access token is missing or invalid");
                    return;
                }

                var urlUsername = GetUsernameFromUrl(e);
                if (urlUsername != username)
                {
                    e.Reply(403,
                        "Access denied"); // 403 Forbidden if the user is trying to update another user's profile
                    return;
                }

                var updatedUserData = JsonConvert.DeserializeObject<UserData>(e.Payload);
                if (updatedUserData == null)
                {
                    e.Reply(400, "Invalid request");
                    return;
                }

                bool updateSuccessful = _userService.UpdateUserProfile(username, updatedUserData);
                if (updateSuccessful)
                {
                    var userData = _userService.RetrieveUserDataInUsers(username);
                    if (userData != null)
                    {
                        // Send updated user data
                        string jsonResponse = JsonConvert.SerializeObject(userData);
                        e.Reply(200, jsonResponse);
                    }
                    else
                    {
                        e.Reply(500, "Internal Server Error");
                    }
                }
                else
                {
                    e.Reply(404, "User not found");
                }
            }
            catch (Exception)
            {
                e.Reply(401, "Access token is missing or invalid");
            }
        }

        private string GetUsernameFromUrl(HttpSvrEventArgs e)
        {
            var segments =
                e.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries); // Split the path into segments
            for (int i = 0; i < segments.Length; i++)
            {
                if (segments[i].Equals("users", StringComparison.OrdinalIgnoreCase) && i + 1 < segments.Length)
                {
                    return segments[i + 1]; // Return the next segment after "users", which should be the username
                }
            }

            return null; // Return null if the username is not found in the URL
        }

        private bool IsAuthenticated(HttpSvrEventArgs e)
        {
            var authHeader = e.Headers.FirstOrDefault(h => h["Authorization"] != null);
            var token = authHeader?["Authorization"];

            if (string.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
            {
                return false;
            }

            // Extract the JWT token
            var jwtToken = token.Substring("Bearer ".Length).Trim();
            // Validate the token
            return ValidateJwtToken(jwtToken);
        }

        private bool ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false, // Validate other aspects of the token as needed, e.g., lifetime
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero // reduce or eliminate clock skew tolerance
                }, out SecurityToken validatedToken);
                // The token is valid
                return true;
            }
            catch
            {
                // Token is not valid
                return false;
            }
        }

        private string GetUsernameFromToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                Console.WriteLine("Token is null or whitespace");
                throw new ArgumentException("Token is null or whitespace", nameof(token));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            if (jwtToken == null)
            {
                Console.WriteLine("JWT Token is null");
                return null;
            }

            // Check for the "unique_name" claim which holds the username in token.
            var usernameClaim = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "unique_name");
            return usernameClaim?.Value;
        }

        private string GetTokenFromHeaders(HttpHeader[] headers)
        {
            foreach (var header in headers)
            {
                if (header.ContainsKey("Authorization"))
                {
                    var token = header["Authorization"];
                    if (!string.IsNullOrEmpty(token) && token.StartsWith("Bearer "))
                    {
                        return token.Substring("Bearer ".Length).Trim();
                    }
                }
            }

            return null; // Return null if no token is found
        }
    }
}