using MonsterTradingCardsGame.HTTP;
using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Interfaces;
using Newtonsoft.Json;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace MonsterTradingCardsGame.Controller
{
    public class PackageController
    {
        private readonly IPackageService _packageService;
        private readonly IUserService _userService;

        public PackageController(IPackageService packageService, IUserService userService)
        {
            _packageService = packageService;
            _userService = userService;
        }

        public void ProcessMessage(HttpSvrEventArgs e)
        {
            switch (e.Method)
            {
                case "POST":
                    if (e.Path == "/packages")
                        CreatePackage(e);
                    else if (e.Path == "/transactions/packages")
                        AcquirePackage(e);
                    break;
            }
        }

        private void CreatePackage(HttpSvrEventArgs e)
        {
            if (!IsAuthenticated(e))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            if (!IsAdmin(e))
            {
                e.Reply(403, "Provided user is not 'admin'");
                return;
            }

            try
            {
                List<CardDTO> cards;
                // deserializing directly into a List<CardDTO>
                try
                {
                    cards = JsonConvert.DeserializeObject<List<CardDTO>>(e.Payload);
                }
                catch (JsonException)
                {
                    // If direct deserialization fails, deserializing into PackageDTO
                    var packageDto = JsonConvert.DeserializeObject<PackageDTO>(e.Payload);
                    if (packageDto?.Cards == null)
                    {
                        throw; // Rethrow the original JsonException
                    }

                    cards = packageDto.Cards;
                }

                if (cards.Count == 0)
                {
                    e.Reply(400, "Bad Request: Invalid package data.");
                    return;
                }

                var package = new Package(cards.Select(dto => dto.CreateCard()).ToList());

                if (_packageService.CreatePackage(package, out string message))
                {
                    e.Reply(201, message);
                }
                else
                {
                    e.Reply(409, message); // Failure with detailed message
                }
            }
            catch (JsonException ex)
            {
                // Log or handle the error appropriately
                Console.WriteLine("JSON deserialization error: " + ex.Message);
                e.Reply(400, "Bad Request: Invalid JSON format.");
            }
            catch (Exception ex)
            {
                // Log or handle the error appropriately
                Console.WriteLine("Unhandled exception: " + ex.Message);
                e.Reply(500, "Internal Server Error");
            }
        }

        private bool IsAuthenticated(HttpSvrEventArgs e)
        {
            var authHeader = e.Headers.FirstOrDefault(h => h["Authorization"] != null);
            var token = authHeader?["Authorization"];

            if (string.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
            {
                return false;
            }

            // Extract the JWT token
            var jwtToken = token.Substring("Bearer ".Length).Trim();
            // Validate the token
            return ValidateJwtToken(jwtToken);
        }

        private bool ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false, // Validate other aspects of the token as needed, e.g., lifetime
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero // reduce or eliminate clock skew tolerance
                }, out SecurityToken validatedToken);
                // The token is valid
                return true;
            }
            catch
            {
                // Token is not valid
                return false;
            }
        }

        private bool IsAdmin(HttpSvrEventArgs e)
        {
            var token = GetTokenFromHeaders(e.Headers);
            var username = GetUsernameFromToken(token);
            return username == "admin"; // Check if the username is 'admin'
        }

        private string GetUsernameFromToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                Console.WriteLine("Token is null or whitespace");
                throw new ArgumentException("Token is null or whitespace", nameof(token));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            if (jwtToken == null)
            {
                Console.WriteLine("JWT Token is null");
                return null;
            }

            // Check for the "unique_name" claim which holds the username in token.
            var usernameClaim = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "unique_name");
            return usernameClaim?.Value;
        }

        private void AcquirePackage(HttpSvrEventArgs e)
        {
            if (!IsAuthenticated(e))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var userId = GetUserIdFromToken(GetTokenFromHeaders(e.Headers));
            if (string.IsNullOrEmpty(userId))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            try
            {
                if (!_packageService.HasEnoughMoney(userId))
                {
                    e.Reply(403, "Not enough money for buying a card package");
                    return;
                }
                else if (!_packageService.IsPackageAvailable(userId))
                {
                    e.Reply(404, "No card package available for buying");
                    return;
                }
                else if (_packageService.ProcessPurchase(userId))
                {
                    var cards = _packageService.GetCardsFromLastPurchasedPackage(userId);
                    var jsonResponse = JsonConvert.SerializeObject(cards);
                    e.Reply(200, jsonResponse);
                }
                else
                {
                    e.Reply(400, "Failed to process the purchase");
                }
            }
            catch (Exception ex)
            {
                e.Reply(500, $"Internal Server Error: {ex.Message}");
            }
        }

        private string GetUserIdFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };

                var principal =
                    tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;

                // Use the same claim type that was used when generating the token
                // This should match the claim type in the GenerateJwtToken method
                var userIdClaim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                if (userIdClaim != null)
                {
                    return userIdClaim.Value;
                }
            }
            catch (Exception ex)
            {
                // Log the exception details for debugging purposes
                Console.WriteLine($"Token validation failed: {ex.Message}");
                return null;
            }

            return null;
        }

        private string GetTokenFromHeaders(HttpHeader[] headers)
        {
            foreach (var header in headers)
            {
                if (header.ContainsKey("Authorization"))
                {
                    var token = header["Authorization"];
                    if (!string.IsNullOrEmpty(token) && token.StartsWith("Bearer "))
                    {
                        return token.Substring("Bearer ".Length).Trim();
                    }
                }
            }

            return null; // Return null if no token is found
        }
    }
}