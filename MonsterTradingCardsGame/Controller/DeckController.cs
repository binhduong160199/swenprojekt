using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.HTTP;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Controller
{
    public class DeckController
    {
        private readonly IDeckService _deckService;
        private readonly IUserService _userService;

        public DeckController(IDeckService deckService, IUserService userService)
        {
            _deckService = deckService;
            _userService = userService;
        }

        public void ProcessMessage(HttpSvrEventArgs e)
        {
            if (!IsAuthenticated(e))
            {
                e.Reply(401, "Unauthorized");
                return;
            }

            switch (e.Method)
            {
                case "GET":
                    var pathAndQuery = e.Path.Split('?');
                    var path = pathAndQuery[0];
                    var queryString = pathAndQuery.Length > 1 ? pathAndQuery[1] : "";

                    if (path == "/deck")
                    {
                        if (queryString.Contains("format=plain"))
                        {
                            GetDeckPlainText(e);
                        }
                        else
                        {
                            GetDeck(e);
                        }
                    }

                    break;
                case "PUT":
                    ConfigureDeck(e);
                    break;
                default:
                    e.Reply(405, "Method Not Allowed");
                    break;
            }
        }

        private void GetDeck(HttpSvrEventArgs e)
        {
            var username = GetUsernameFromToken(GetTokenFromHeaders(e.Headers));
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var deck = _deckService.GetDeckByUsername(username);
            if (deck == null || deck.Count == 0)
            {
                e.Reply(204, "The request was fine, but the deck doesn't have any cards");
                return;
            }

            string jsonResponse = JsonConvert.SerializeObject(deck);
            e.Reply(200, jsonResponse);
        }

        private void GetDeckPlainText(HttpSvrEventArgs e)
        {
            var username = GetUsernameFromToken(GetTokenFromHeaders(e.Headers));
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            var deck = _deckService.GetDeckByUsername(username);
            if (deck == null || deck.Count == 0)
            {
                e.Reply(204, "The request was fine, but the deck doesn't have any cards");
                return;
            }

            var plainTextResponse = ConvertDeckToPlainText(deck);
            e.Reply(200, plainTextResponse);
        }

        private string ConvertDeckToPlainText(List<CardDTO> cards)
        {
            // Format the card data as plain text
            return string.Join(Environment.NewLine,
                cards.Select(cards => $"Id: {cards.Id}, Name: {cards.Name}, Damage: {cards.Damage}"));
        }

        private void ConfigureDeck(HttpSvrEventArgs e)
        {
            var username = GetUsernameFromToken(GetTokenFromHeaders(e.Headers));
            if (string.IsNullOrEmpty(username))
            {
                e.Reply(401, "Access token is missing or invalid");
                return;
            }

            string[] cardIds = JsonConvert.DeserializeObject<string[]>(e.Payload);
            if (cardIds.Length != 4)
            {
                e.Reply(400, "The provided deck did not include the required amount of cards");
                return;
            }

            List<string> cardIdList = cardIds.ToList();

            // Check if the deck is already configured with these cards
            if (_deckService.IsDeckAlreadyConfigured(cardIdList))
            {
                e.Reply(409, $"The deck is already configured with these cards");
                return;
            }

            bool success = _deckService.ConfigureDeck(username, cardIdList);
            if (success)
            {
                e.Reply(200, "The deck has been successfully configured");
            }
            else
            {
                e.Reply(403, "At least one of the provided cards does not belong to the user or is not available");
            }
        }

        private bool IsAuthenticated(HttpSvrEventArgs e)
        {
            var authHeader = e.Headers.FirstOrDefault(h => h["Authorization"] != null);
            var token = authHeader?["Authorization"];

            if (string.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
            {
                return false;
            }

            // Extract the JWT token
            var jwtToken = token.Substring("Bearer ".Length).Trim();
            // Validate the token
            return ValidateJwtToken(jwtToken);
        }

        private bool ValidateJwtToken(string jwtToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("mySuperSecretKeyForJwtTokenGeneration2024!");

            try
            {
                tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false, // Validate other aspects of the token as needed, e.g., lifetime
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero // reduce or eliminate clock skew tolerance
                }, out SecurityToken validatedToken);
                // The token is valid
                return true;
            }
            catch
            {
                // Token is not valid
                return false;
            }
        }

        private string GetUsernameFromToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                Console.WriteLine("Token is null or whitespace");
                throw new ArgumentException("Token is null or whitespace", nameof(token));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            if (jwtToken == null)
            {
                Console.WriteLine("JWT Token is null");
                return null;
            }

            // Check for the "unique_name" claim which holds the username in token.
            var usernameClaim = jwtToken.Claims.FirstOrDefault(claim => claim.Type == "unique_name");
            return usernameClaim?.Value;
        }

        private string GetTokenFromHeaders(HttpHeader[] headers)
        {
            foreach (var header in headers)
            {
                if (header.ContainsKey("Authorization"))
                {
                    var token = header["Authorization"];
                    if (!string.IsNullOrEmpty(token) && token.StartsWith("Bearer "))
                    {
                        return token.Substring("Bearer ".Length).Trim();
                    }
                }
            }

            return null; // Return null if no token is found
        }
    }
}