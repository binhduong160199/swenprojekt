﻿using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool RegisterUser(string username, string password)
        {
            if (_userRepository.GetUserByUsername(username) != null)
            {
                return false; // User already exists
            }

            var passwordHash = HashPassword(password);
            var newUser = new UserCredentials(username, passwordHash);
            _userRepository.CreateUser(newUser);
            return true;
        }

        private string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public UserCredentials RetrieveUserData(string username)
        {
            return _userRepository.GetUserByUsername(username);
        }

        public UserData RetrieveUserDataInUsers(string username)
        {
            return _userRepository.GetUserDataByUsername(username);
        }

        public bool UpdateUserProfile(string username, UserData updatedUserData)
        {
            // Check if the user exists
            var existingUser = _userRepository.GetUserByUsername(username);
            if (existingUser == null)
            {
                return false; // User not found
            }

            // Update the user profile in the database
            return _userRepository.UpdateUserProfile(username, updatedUserData);
        }

        public bool AuthenticateUser(string username, string password)
        {
            // Retrieve the stored password hash from the database
            var storedPasswordHash = _userRepository.GetPasswordHashByUsername(username);

            if (storedPasswordHash != null)
            {
                // Compare the provided password with the stored password hash
                return VerifyPasswordHash(password, storedPasswordHash);
            }

            return false; // User not found or password mismatch
        }

        private bool VerifyPasswordHash(string password, string storedPasswordHash)
        {
            // Use BCrypt to verify the password against the stored hash
            return BCrypt.Net.BCrypt.Verify(password, storedPasswordHash);
        }

        public bool DoesUserExist(string username)
        {
            return _userRepository.DoesUserExist(username);
        }
    }
}