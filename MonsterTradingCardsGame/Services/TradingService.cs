using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Services;

public class TradingService : ITradingService
{
    private readonly ITradingRepository _tradingRepository;

    public TradingService(ITradingRepository tradingRepository)
    {
        _tradingRepository = tradingRepository;
    }

    public List<TradingDeal> GetTradingDeal(string username)
    {
        return _tradingRepository.GetTradingDeal(username);
    }

    public bool CheckCardIfUserNotOwnOrInDeck(TradingDeal tradingDeal, string username)
    {
        return _tradingRepository.CheckCardIfUserNotOwnOrInDeck(tradingDeal, username);
    }

    public bool CheckDealExist(TradingDeal tradingDeal, string username)
    {
        return _tradingRepository.CheckDealExist(tradingDeal, username);
    }

    public void CreateTrading(TradingDeal tradingDeal)
    {
        _tradingRepository.CreateTrading(tradingDeal);
    }

    public bool UserHasDeal(Guid tradingId, string username)
    {
        return _tradingRepository.UserHasDeal(tradingId, username);
    }

    public bool FindDeal(Guid tradingId)
    {
        return _tradingRepository.FindDeal(tradingId);
    }

    public void DeleteTrading(Guid tradingId)
    {
        _tradingRepository.DeleteTrading(tradingId);
    }

    public bool IfCardIsGoodEnoughOrNotYourOwnCard(Guid tradingId, string username)
    {
        return _tradingRepository.IfCardIsGoodEnoughOrNotYourOwnCard(tradingId, username);
    }

    public void DoTrading(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete)
    {
        _tradingRepository.DoTrading(tradingIdHas, tradingIdWantToTrade, idToDelete);
    }

    public Guid FindTheCardOfTradingId(Guid tradingId)
    {
        return _tradingRepository.FindTheCardOfTradingId(tradingId);
    }

    public bool UserTryToTradeToHimSelf(Guid tradingId, string username)
    {
        return _tradingRepository.UserTryToTradeToHimSelf(tradingId, username);
    }
}