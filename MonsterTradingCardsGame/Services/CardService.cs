using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Interfaces;

namespace MonsterTradingCardsGame.Services
{
    public class CardService : ICardService
    {
        private readonly ICardRepository _cardRepository;

        public CardService(ICardRepository cardRepository)
        {
            _cardRepository = cardRepository;
        }

        public List<CardDTO> GetCardsByUsername(string username)
        {
            return _cardRepository.GetCardsByUsername(username);
        }
    }
}