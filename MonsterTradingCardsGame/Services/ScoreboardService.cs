using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Services;

public class ScoreboardService : IScoreboardService
{
    private readonly IScoreboardRepository _scoreboardRepository;

    public ScoreboardService(IScoreboardRepository scoreboardRepository)
    {
        _scoreboardRepository = scoreboardRepository;
    }

    public List<UserStats> GetUserScoreboard()
    {
        return _scoreboardRepository.GetUserScoreboardByUserElo();
    }
}