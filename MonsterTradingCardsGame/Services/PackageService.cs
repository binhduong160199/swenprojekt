using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Services
{
    public class PackageService : IPackageService
    {
        private readonly IPackageRepository _packageRepository;
        private readonly IUserRepository _userRepository;
        private readonly Dictionary<string, int> _userPackageCounts = new Dictionary<string, int>();
        private const int MaxPackagesPerUser = 4;

        public PackageService(IPackageRepository packageRepository, IUserRepository userRepository)
        {
            _packageRepository = packageRepository;
            _userRepository = userRepository;
        }

        public bool CreatePackage(Package package, out string message)
        {
            try
            {
                // Check if any card in the package already exists in the database
                foreach (var card in package.Cards)
                {
                    if (_packageRepository.CardExists(card.Id))
                    {
                        message = $"Card with ID {card.Id} already exists in a package";
                        return false;
                    }
                }

                // Add the package to the repository (database)
                _packageRepository.AddPackage(package);

                message = "Package created successfully";
                return true;
            }
            catch (Exception ex)
            {
                // Log the exception (consider using a logging framework)
                message = $"Failed to create package: {ex.Message}";
                return false;
            }
        }

        public bool IsPackageAvailable(string userId)
        {
            // Check if the user has acquired less than the maximum number of packages
            _userPackageCounts.TryGetValue(userId, out var acquiredPackageCount);
            if (acquiredPackageCount >= MaxPackagesPerUser)
            {
                return false; // The user has already acquired the maximum number of packages
            }

            // Use the new method in PackageRepository to check for unacquired cards
            return _packageRepository.AreUnacquiredCardsAvailable();
        }

        public bool HasEnoughMoney(string userId)
        {
            int cardsOwned = _packageRepository.GetNumberOfCardsOwnedByUser(userId);
            int maxCards = 20; // Since each user can acquire up to 20 cards (4 packages)
            return cardsOwned < maxCards;
        }

        public bool ProcessPurchase(string userId)
        {
            // Select a package for the user
            // GetAvailablePackage returns a random available package
            var package = _packageRepository.GetAvailablePackage();
            if (package == null)
            {
                return false; // No packages available
            }

            // Add the cards from the package to the user's card collection
            foreach (var card in package.Cards)
            {
                _packageRepository.AddCardToUser(userId, card.Id);
            }

            // Record the package acquisition
            RecordPackageAcquisition(userId);
            return true;
        }

        // Method to be called when a user acquires a package
        public void RecordPackageAcquisition(string userId)
        {
            if (_userPackageCounts.ContainsKey(userId))
            {
                _userPackageCounts[userId]++;
            }
            else
            {
                _userPackageCounts[userId] = 1;
            }
        }

        public List<CardDTO> GetCardsFromLastPurchasedPackage(string username)
        {
            return _packageRepository.GetCardsFromLastPurchasedPackage(username);
        }
    }
}