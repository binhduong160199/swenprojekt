using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Services
{
    public class DeckService : IDeckService
    {
        private readonly IDeckRepository _deckRepository;
        private readonly ICardRepository _cardRepository;
        private readonly IPackageRepository _packageRepository;

        public DeckService(IDeckRepository deckRepository, ICardRepository cardRepository,
            IPackageRepository packageRepository)
        {
            _deckRepository = deckRepository;
            _cardRepository = cardRepository;
            _packageRepository = packageRepository;
        }

        public bool ConfigureDeck(string username, List<string> cardIds)
        {
            return _deckRepository.ConfigureDeck(username, cardIds);
        }

        public List<CardDTO> GetDeckByUsername(string username)
        {
            return _deckRepository.GetDeckByUsername(username);
        }

        public User GetUserAndCardInDeck(string username)
        {
            //int userId = GetUserId(username);
            List<CardDTO> deck = _cardRepository.GetCardsByUsernameForBattle(username);

            UserStats stats = _deckRepository.GetUserStatsByUsername(username);

            // Ensure the deck array has the correct size (4 in this case)
            CardDTO[] deckArray = deck.Take(4).ToArray();

            return new User(username, deckArray, stats);
        }

        // In DeckService
        public bool IsDeckAlreadyConfigured(List<string> cardIdList)
        {
            return _deckRepository.DoesDeckConfigurationExist(cardIdList);
        }
    }
}