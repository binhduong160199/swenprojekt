using System.Net.Sockets;
using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Interfaces;
using System.Text;

namespace MonsterTradingCardsGame.Services
{
    public class BattleService : IBattleService
    {
        private List<TcpClient> _clients = new List<TcpClient>();
        private ManualResetEvent _lobbyDoneEvent = new ManualResetEvent(false);

        private User? User1 = null;
        private User? User2 = null;

        private List<CardDTO> Team1 { get; set; }
        private List<CardDTO> Team2 { get; set; }

        private string _userName1 = string.Empty;

        private string _userName2 = string.Empty;

        private int RoundCount { get; set; }
        private SemaphoreSlim _lobbySemaphore = new SemaphoreSlim(1, 1); // Initialize SemaphoreSlim

        private readonly IBattleRepository _battlesRepository;

        public BattleService(IBattleRepository battlesRepository)
        {
            _battlesRepository = battlesRepository;
            Team1 = new List<CardDTO>();
            Team2 = new List<CardDTO>();
            RoundCount = 0;
        }

        public async Task<string> EnterLobbyAsync(User user, TcpClient client)
        {
            await _lobbySemaphore.WaitAsync(); // Asynchronously wait to enter the semaphore

            try
            {
                if (_clients.Count == 0)
                {
                    _clients.Add(client);
                    User1 = user;
                    _userName1 = user.Username;
                    Console.WriteLine("Player 1 Entered");
                    return "Waiting for player 2 to join...";
                }
                else if (_clients.Count == 1)
                {
                    _clients.Add(client);
                    User2 = user;
                    _userName2 = user.Username;
                    Console.WriteLine("Player 2 Entered");
                    Team1 = User1.Deck.ToList();
                    Team2 = User2.Deck.ToList();
                    _lobbyDoneEvent.Reset();

                    // Start the game and return its result
                    return await StartGame(_userName1, _userName2);
                }
                else
                {
                    return "Lobby is full";
                }
            }
            finally
            {
                _lobbySemaphore.Release(); // Release the semaphore
            }
        }

        public void WaitsForGame()
        {
            _lobbyDoneEvent.WaitOne();
        }

        private async Task<string> StartGame(string userName1, string userName2)
        {
            StringBuilder battleLog = new StringBuilder();
            while (!IsGameOver())
            {
                string roundResult = await ExecuteRoundAsync(userName1, userName2);
                battleLog.AppendLine(roundResult); // Appending the round log to the battleLog
                Console.WriteLine(roundResult); // Keeping the console log for each round
            }

            string outcome = DetermineOutcome(userName1, userName2);
            battleLog.AppendLine(outcome); // Appending the final outcome to the battleLog
            Console.WriteLine(outcome); // Keeping the console log for the final outcome

            // Handle post-game actions based on the outcome
            if (outcome == "Team 1 Wins\n")
            {
                _battlesRepository.ChangeStatsAfterBattle(userName1, userName2);
            }
            else if (outcome == "Team 2 Wins\n")
            {
                _battlesRepository.ChangeStatsAfterBattle(userName2, userName1);
            }

            _lobbyDoneEvent.Set();
            return battleLog.ToString();
        }

        public bool IsGameOver()
        {
            return Team1.Count == 0 || Team2.Count == 0 || RoundCount >= 100;
        }

        public async Task<string> ExecuteRoundAsync(string username1, string username2)
        {
            StringBuilder roundLog = new StringBuilder();
            if (Team1.Any() && Team2.Any())
            {
                // Create a Random instance
                Random random = new Random();
                int randomIndexTeam1 = random.Next(0, 4); // 4 is exclusive
                int randomIndexTeam2 = random.Next(0, 4); // 4 is exclusive

                var Card = new CardDTO();
                // Select a card from each team
                Card team1Card = Card.CreateCard(Team1[randomIndexTeam1].Id, Team1[randomIndexTeam1].Name,
                    Team1[randomIndexTeam1].Damage);
                Card team2Card = Card.CreateCard(Team2[randomIndexTeam2].Id, Team2[randomIndexTeam2].Name,
                    Team2[randomIndexTeam2].Damage);

                // Asynchronous, waiting for player's input, delay, etc.
                await Task.Delay(0); // This is a placeholder for actual async logic

                // Perform attack
                var (outcome1, Damage1, opponentDamage1) = team1Card.Attack(team2Card);
                var (outcome2, Damage2, opponentDamage2) = team2Card.Attack(team1Card);

                // Log the attack results

                roundLog.AppendLine($"PlayerA: {team1Card.Name} ({Damage1} Damage) " +
                                    $"vs PlayerB: {team2Card.Name} ({Damage2} Damage)" + " => " +
                                    $"{Damage1} vs {Damage2}" + " -> " + $"{opponentDamage1} vs {opponentDamage2}");
                Console.WriteLine($"PlayerA: {team1Card.Name} ({Damage1} Damage) " +
                                  $"vs PlayerB: {team2Card.Name} ({Damage2} Damage)" + " => " +
                                  $"{Damage1} vs {Damage2}" + " -> " + $"{opponentDamage1} vs {opponentDamage2}");

                // Outcome

                roundLog.AppendLine(HandleBattleOutcome(team1Card, team2Card, outcome1, outcome2, username1,
                    username2));
                RoundCount++;
                Console.WriteLine("IN ROUND: " + RoundCount);
                roundLog.AppendLine("IN ROUND:" + RoundCount);
                return roundLog.ToString();
            }

            return "";
        }

        public string HandleBattleOutcome(Card team1Card, Card team2Card, string team1Result, string team2Result,
            string username1, string username2)
        {
            StringBuilder outcomeLog = new StringBuilder();
            if (team1Result == "win" && team2Result != "win")
            {
                Console.WriteLine($"{team1Card.Name} wins");
                // remove card from deck
                _battlesRepository.RemoveLoseCardFromDeck(team2Card);
            }
            else if (team2Result == "win" && team1Result != "win")
            {
                Console.WriteLine($"{team2Card.Name} wins");
                // remove card from deck
                _battlesRepository.RemoveLoseCardFromDeck(team1Card);
            }
            else
            {
                Console.WriteLine("No Card wins this round");
            }

            return outcomeLog.ToString();
        }

        public string DetermineOutcome(string username1, string username2)
        {
            int team1 = _battlesRepository.CountCardInDeck(username1);
            int team2 = _battlesRepository.CountCardInDeck(username2);
            if (team1 > team2)
            {
                return "Team 1 Wins\n";
            }

            if (team2 > team1)
            {
                return "Team 2 Wins\n";
            }

            return "Draw";
        }

        public void RemoveLoseCardFromDeck(Card card)
        {
            _battlesRepository.RemoveLoseCardFromDeck(card);
        }

        public int CountCardInDeck(string username)
        {
            return _battlesRepository.CountCardInDeck(username);
        }
    }
}