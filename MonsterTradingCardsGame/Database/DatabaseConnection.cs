﻿using Npgsql;

namespace MonsterTradingCardsGame.Database
{
    public class DatabaseConnection
    {
        private string _connectionString = "Host=127.0.0.1;Port=5432;Username=postgres;Password=postgres;Database=mydb";

        public NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(_connectionString);
        }
        public bool TestDatabaseConnection()
        {
            try
            {
                using (var connection = GetConnection())
                {
                    connection.Open(); // Attempt to open the connection
                    return connection.State == System.Data.ConnectionState.Open; // Check if the connection is open
                }
            }
            catch (NpgsqlException ex)
            {
                Console.WriteLine($"Error connecting to the database: {ex.Message}");
                return false;
            }
        }
    }
}