﻿using Npgsql;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.Repositories
{
    public class UserRepository : IUserRepository
    {
        // readdonly is assigned only one and remains unchanged
        // This preserves the integrity of the data.
        private readonly DatabaseConnection _dbConnection;

        public UserRepository(DatabaseConnection dbConnection)
        {
            // only created in constructor and unchanged
            _dbConnection = dbConnection;
        }

        public bool TestDatabaseConnection()
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open(); // Attempt to open the connection
                    return connection.State == System.Data.ConnectionState.Open; // Check if the connection is open
                }
            }
            catch (NpgsqlException ex)
            {
                Console.WriteLine($"Error connecting to the database: {ex.Message}");
                return false;
            }
        }

        public void CreateUser(UserCredentials user)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                using (var trans = conn.BeginTransaction())
                {
                    // Insert into users table first
                    using (var cmd = new NpgsqlCommand(
                               "INSERT INTO users (username, name, bio, image) VALUES (@username, '', '', '')", conn,
                               trans))
                    {
                        cmd.Parameters.AddWithValue("@username", user.Username);
                        cmd.ExecuteNonQuery();
                    }

                    // Then insert into user_credentials
                    using (var cmd = new NpgsqlCommand(
                               "INSERT INTO user_credentials (username, password_hash) VALUES (@username, @password_hash)",
                               conn, trans))
                    {
                        cmd.Parameters.AddWithValue("@username", user.Username);
                        cmd.Parameters.AddWithValue("@password_hash", user.Password);
                        cmd.ExecuteNonQuery(); //executes a SQL command that doesn't return any data, such as an INSERT, UPDATE, or DELETE statement.
                    }

                    // Then insert into user_stats
                    using (var cmd = new NpgsqlCommand(
                               "INSERT INTO user_stats (username, elo, wins, losses) VALUES (@Username, @Elo, @Wins, @Losses)",
                               conn, trans))
                    {
                        cmd.Parameters.AddWithValue("@username", user.Username);
                        cmd.Parameters.AddWithValue("@Elo", 100);
                        cmd.Parameters.AddWithValue("@Wins", 0);
                        cmd.Parameters.AddWithValue("@Losses", 0);
                        cmd.ExecuteNonQuery(); //executes a SQL command that doesn't return any data, such as an INSERT, UPDATE, or DELETE statement.
                    }

                    trans.Commit(); //transaction are completed successfully and saved to the database. 
                }
            }
        }

        public UserCredentials GetUserByUsername(string username)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(
                           "SELECT username, password_hash FROM user_credentials WHERE username = @username", conn))
                {
                    cmd.Parameters.AddWithValue("@username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new UserCredentials(
                                reader.GetString(reader.GetOrdinal("username")),
                                reader.GetString(reader.GetOrdinal("password_hash"))
                            );
                        }
                    }
                }
            }

            return null; // User not found
        }

        public bool UpdateUserProfile(string username, UserData updatedUserData)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(
                           "UPDATE users SET name = @name, bio = @bio, image = @image WHERE username = @username",
                           conn))
                {
                    cmd.Parameters.AddWithValue("@name", updatedUserData.Name);
                    cmd.Parameters.AddWithValue("@bio", updatedUserData.Bio);
                    cmd.Parameters.AddWithValue("@image", updatedUserData.Image);
                    cmd.Parameters.AddWithValue("@username", username);

                    int affectedRows = cmd.ExecuteNonQuery();
                    return affectedRows > 0;
                }
            }
        }

        public string GetPasswordHashByUsername(string username)
        {
            string passwordHash = null;

            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(
                           "SELECT password_hash FROM user_credentials WHERE username = @username", conn))
                {
                    cmd.Parameters.AddWithValue("@username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            passwordHash = reader["password_hash"].ToString();
                        }
                    }
                }
            }
            return passwordHash;
        }

        public UserData? GetUserDataByUsername(string username)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(
                           "SELECT username, name, bio, image FROM users WHERE username = @username", conn))
                {
                    cmd.Parameters.AddWithValue("@username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new UserData(
                                reader.GetString(reader.GetOrdinal("username")),
                                reader.GetString(reader.GetOrdinal("name")),
                                reader.IsDBNull(reader.GetOrdinal("bio"))
                                    ? null
                                    : reader.GetString(reader.GetOrdinal("bio")),
                                reader.IsDBNull(reader.GetOrdinal("image"))
                                    ? null
                                    : reader.GetString(reader.GetOrdinal("image"))
                            );
                        }
                    }
                }
            }

            return null; // User not found
        }

        public bool DoesUserExist(string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    var query = @"
                SELECT EXISTS (
                    SELECT 1
                    FROM users
                    WHERE username = @Username
                );";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);

                        var result = (bool)command.ExecuteScalar();
                        return result; // True if the username exists in user_cards, false otherwise
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in UsernameExistsInUserCards: {ex.Message}");
                return false;
            }
        }
    }
}