using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Interfaces;
using Npgsql;
using NpgsqlTypes;

namespace MonsterTradingCardsGame.Repositories
{
    public class PackageRepository : IPackageRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public PackageRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public void AddPackage(Package package)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                using (var transaction = conn.BeginTransaction())
                {
                    foreach (var card in package.Cards)
                    {
                        if (!CardExists(card.Id))
                        {
                            // Insert the card into the database if it doesn't exist
                            var insertCardQuery = @"
                    INSERT INTO cards (card_id, name, damage)
                    VALUES (@CardId, @Name, @Damage)";
                            using (var insertCmd = new NpgsqlCommand(insertCardQuery, conn, transaction))
                            {
                                insertCmd.Parameters.AddWithValue("@CardId", card.Id);
                                insertCmd.Parameters.AddWithValue("@Name", card.Name);
                                insertCmd.Parameters.AddWithValue("@Damage", card.Damage);
                                insertCmd.ExecuteNonQuery();
                            }
                        }
                    }

                    // Now insert the package
                    var insertPackageQuery = @"
            INSERT INTO packages (package_id, card_id1, card_id2, card_id3, card_id4, card_id5)
            VALUES (@PackageId, @CardId1, @CardId2, @CardId3, @CardId4, @CardId5)";
                    using (var packageCmd = new NpgsqlCommand(insertPackageQuery, conn, transaction))
                    {
                        packageCmd.Parameters.AddWithValue("@PackageId", package.Id);

                        for (int i = 0; i < 5; i++)
                        {
                            var cardIdParam = $"@CardId{i + 1}";
                            var cardIdValue = i < package.Cards.Count ? package.Cards[i].Id : (object)DBNull.Value;
                            packageCmd.Parameters.AddWithValue(cardIdParam, cardIdValue);
                        }

                        packageCmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }
        }

        public bool CardExists(Guid cardId)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand("SELECT EXISTS (SELECT 1 FROM cards WHERE card_id = @CardId)", conn))
                {
                    cmd.Parameters.AddWithValue("@CardId", cardId);

                    bool exists = (bool)cmd.ExecuteScalar();
                    return exists;
                }
            }
        }

        public int GetNumberOfCardsOwnedByUser(string userId)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = "SELECT COUNT(*) FROM user_cards WHERE username = @UserId";
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }

        public Package GetAvailablePackage()
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
SELECT p.*
FROM packages p
WHERE NOT EXISTS (
    SELECT 1 FROM user_cards uc WHERE uc.card_id IN (p.card_id1, p.card_id2, p.card_id3, p.card_id4, p.card_id5)
)
ORDER BY RANDOM()
LIMIT 1;
";
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var packageId = reader.GetGuid(0);
                            var cards = new List<Card>(); // List to hold cards

                            for (int i = 1; i <= 5; i++)
                            {
                                if (!reader.IsDBNull(i))
                                {
                                    var cardId = reader.GetGuid(i);
                                    var card = GetCardById(cardId);
                                    if (card != null)
                                    {
                                        cards.Add(card);
                                    }
                                }
                            }

                            // Return a package with the retrieved ID and list of cards
                            return new Package(packageId, cards);
                        }
                    }
                }
            }

            return null; // Return null if no package is found
        }

        public Card GetCardById(Guid cardId)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
            SELECT card_id, name, damage
            FROM cards
            WHERE card_id = @CardId";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    // Explicitly specify the type of the parameter
                    cmd.Parameters.Add("@CardId", NpgsqlTypes.NpgsqlDbType.Uuid).Value = cardId;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var id = reader.GetGuid(0);
                            var name = reader.GetString(1);
                            var damage = reader.GetDouble(2);
                            var cardType = DetermineCardType(name);

                            // Create the appropriate card based on the card type
                            switch (cardType)
                            {
                                case CardTypeEnum.Spell:
                                    return new SpellCard(id, name, damage);

                                case CardTypeEnum.Monster:
                                default:
                                    return new MonsterCard(id, name, damage);
                            }
                        }
                    }
                }
            }

            return null; // Return null if no card is found
        }

        private CardTypeEnum DetermineCardType(string name)
        {
            // Check if the name contains "Spell" to classify it as a Spell card
            if (name.Contains("Spell", StringComparison.OrdinalIgnoreCase))
            {
                return CardTypeEnum.Spell;
            }
            // Otherwise, classify it as a Monster card by default
            else
            {
                return CardTypeEnum.Monster;
            }
        }

        public void AddCardToUser(string username, Guid cardId)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                // First, check if the card exists
                var checkCardExistsQuery = "SELECT EXISTS (SELECT 1 FROM cards WHERE card_id = @CardId)";
                using (var checkCardExistsCmd = new NpgsqlCommand(checkCardExistsQuery, conn))
                {
                    checkCardExistsCmd.Parameters.Add(new NpgsqlParameter("@CardId", NpgsqlDbType.Uuid)
                        { Value = cardId });

                    bool cardExists = (bool)checkCardExistsCmd.ExecuteScalar();
                    if (!cardExists)
                    {
                        // Handle the error appropriately
                        throw new ArgumentException($"Card with ID {cardId} does not exist.");
                    }
                }

                // If the card exists, proceed to insert it for the user
                var query = @"
INSERT INTO user_cards (username, card_id, acquired_at)
VALUES (@Username, @CardId, NOW())  -- NOW() sets the current timestamp
ON CONFLICT (username, card_id) DO NOTHING;"; // Prevents duplicates

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Username", NpgsqlDbType.Varchar) { Value = username });
                    cmd.Parameters.Add(new NpgsqlParameter("@CardId", NpgsqlDbType.Uuid) { Value = cardId });
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<CardDTO> GetCardsFromLastPurchasedPackage(string username)
        {
            var cards = new List<CardDTO>();
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                var query = @"
            SELECT c.card_id, c.name, c.damage 
            FROM cards c
            INNER JOIN user_cards uc ON c.card_id = uc.card_id
            WHERE uc.username = @Username
            ORDER BY uc.acquired_at DESC
            LIMIT 5";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new CardDTO
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Damage = reader.GetDouble(2),
                            };
                            cards.Add(card);
                        }
                    }
                }
            }
            return cards;
        }

        public bool AreUnacquiredCardsAvailable()
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
SELECT EXISTS (
    SELECT 1 FROM cards c
    WHERE NOT EXISTS (
        SELECT 1 FROM user_cards uc WHERE uc.card_id = c.card_id
    )
) AS available;";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    var result = (bool)cmd.ExecuteScalar();
                    return result; // Returns true if there are unacquired cards, false otherwise
                }
            }
        }
    }
}