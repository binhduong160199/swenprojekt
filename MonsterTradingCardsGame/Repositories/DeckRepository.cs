using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Database;
using Npgsql;

namespace MonsterTradingCardsGame.Repositories
{
    public class DeckRepository : IDeckRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public DeckRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public bool ConfigureDeck(string username, List<string> cardIds)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        // Clear current deck
                        var clearDeckCmd = new NpgsqlCommand("DELETE FROM deck WHERE username = @Username", conn);
                        clearDeckCmd.Parameters.AddWithValue("@Username", username);
                        clearDeckCmd.ExecuteNonQuery();

                        // Add new cards to deck
                        foreach (var cardId in cardIds)
                        {
                            var addCardCmd =
                                new NpgsqlCommand(
                                    "INSERT INTO deck (username, card_id) VALUES (@Username, @CardId::uuid)",
                                    conn); // Cast to UUID
                            addCardCmd.Parameters.AddWithValue("@Username", username);
                            addCardCmd.Parameters.AddWithValue("@CardId",
                                cardId); // Assuming cardId is a string representing a valid UUID
                            addCardCmd.ExecuteNonQuery();
                        }

                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        Console.WriteLine($"Failed to configure deck for user {username}: {ex.Message}");
                        return false;
                    }
                }
            }
        }

        public List<CardDTO> GetDeckByUsername(string username)
        {
            var cards = new List<CardDTO>();
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
            SELECT c.card_id, c.name, c.damage
            FROM cards c
            INNER JOIN deck d ON c.card_id = d.card_id
            WHERE d.username = @Username";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new CardDTO
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Damage = reader.GetDouble(2),
                            };
                            cards.Add(card);
                        }
                    }
                }
            }
            return cards;
        }

        public UserStats GetUserStatsByUsername(string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    using (var command =
                           new NpgsqlCommand(
                               "SELECT username, elo, wins, losses FROM user_stats WHERE username = @Username",
                               connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return new UserStats
                                {
                                    Name = reader["username"].ToString(),
                                    Elo = Convert.ToInt32(reader["elo"]),
                                    Wins = Convert.ToInt32(reader["wins"]),
                                    Losses = Convert.ToInt32(reader["losses"])
                                };
                            }
                        }
                    }
                }

                return null; // Return null if user not found
            }
            catch (NpgsqlException ex)
            {
                // Log exception and handle it appropriately
                Console.WriteLine($"Error in GetUserStatsByUsername: {ex.Message}");
                return null;
            }
        }
        
        public bool DoesDeckConfigurationExist(List<string> cardIdList)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    foreach (var cardId in cardIdList)
                    {
                        var query = @"
                    SELECT COUNT(*)
                    FROM deck
                    WHERE card_id = @CardId;";

                        using (var command = new NpgsqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@CardId", Guid.Parse(cardId));

                            var count = (long)command.ExecuteScalar();
                            if (count == 0)
                            {
                                // If any card is not found in the deck table, the configuration does not exist
                                return false;
                            }
                        }
                    }
                    // All cards in the list exist in the deck table
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in DoesDeckConfigurationExist: {ex.Message}");
                return false;
            }
        }
    }
}