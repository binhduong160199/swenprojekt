using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;
using Npgsql;

namespace MonsterTradingCardsGame.Repositories
{
    public class ScoreboardRepository : IScoreboardRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public ScoreboardRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public List<UserStats> GetUserScoreboardByUserElo()
        {
            List<UserStats> userStatsList = new List<UserStats>();

            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command =
                       new NpgsqlCommand("SELECT username, elo, wins, losses FROM user_stats", connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read()) // Iterate over all rows in the result set
                        {
                            var userStats = new UserStats
                            {
                                Name = reader["username"].ToString(),
                                Elo = Convert.ToInt32(reader["elo"]),
                                Wins = Convert.ToInt32(reader["wins"]),
                                Losses = Convert.ToInt32(reader["losses"])
                            };
                            userStatsList.Add(userStats);
                        }
                    }
                }
            }

            return userStatsList; // Return the list of UserStats
        }
    }
}