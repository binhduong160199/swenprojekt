using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Interfaces;
using Npgsql;

namespace MonsterTradingCardsGame.Repositories
{
    public class BattleRepository : IBattleRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public BattleRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public void ChangeStatsAfterBattle(string userWin, string userLose)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    // Update the winning user
                    using (var winCommand =
                           new NpgsqlCommand(
                               "UPDATE user_stats SET elo = elo + 3, wins = wins + 1 WHERE username = @Username",
                               connection))
                    {
                        winCommand.Parameters.AddWithValue("@Username", userWin);
                        winCommand.ExecuteNonQuery();
                    }

                    // Update the losing user
                    using (var loseCommand =
                           new NpgsqlCommand(
                               "UPDATE user_stats SET elo = elo - 5, losses = losses + 1 WHERE username = @Username",
                               connection))
                    {
                        loseCommand.Parameters.AddWithValue("@Username", userLose);
                        loseCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                // Log exception and handle it appropriately
                Console.WriteLine($"Error in ChangeStatsAfterBattle: {ex.Message}");
            }
        }

        public void RemoveLoseCardFromDeck(Card card)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command = new NpgsqlCommand(@"
                DELETE FROM trading_deals
                WHERE card_id = @CardId", connection))
                {
                    command.Parameters.AddWithValue("@CardId", card.Id);
                    int rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected == 0)
                    {
                        Console.WriteLine("No card was removed. ");
                    }
                }
            }
        }

        public int CountCardInDeck(string username)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command = new NpgsqlCommand(@"
            SELECT COUNT(card_id)
            FROM deck
            WHERE username = @Username", connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    // Execute the command and return the card count
                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
        }
    }
}