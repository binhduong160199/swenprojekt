using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Interfaces;
using MonsterTradingCardsGame.Models;
using Npgsql;

namespace MonsterTradingCardsGame.Repositories
{
    public class TradingRepository : ITradingRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public TradingRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public List<TradingDeal> GetTradingDeal(string username)
        {
            var tradingDeals = new List<TradingDeal>();

            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    var query = @"
                SELECT td.card_id, td.type, td.minimum_damage, td.trading_id
                FROM trading_deals td
                INNER JOIN user_cards uc ON td.card_id = uc.card_id
                WHERE uc.username = @Username;";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var tradingDeal = new TradingDeal
                                {
                                    ID = reader.GetGuid(reader.GetOrdinal("trading_id")),
                                    CardToTrade = reader.GetGuid(reader.GetOrdinal("card_id")),
                                    Type = reader["type"].ToString(),
                                    MinimumDamage = reader.GetDouble(reader.GetOrdinal("minimum_damage"))
                                };
                                tradingDeals.Add(tradingDeal);
                            }
                        }
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                Console.WriteLine($"Error in GetTradingDeals: {ex.Message}");
                // Consider how to handle the exception, possibly rethrow or return an empty list
            }

            return tradingDeals;
        }

        public bool CheckCardIfUserNotOwnOrInDeck(TradingDeal tradingDeal, string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
                SELECT 
                CASE 
                    WHEN uc.card_id IS NULL THEN FALSE  -- The user does not own the card
                    WHEN d.card_id IS NOT NULL THEN FALSE -- The card is in the user's deck
                    ELSE TRUE  -- The user owns the card, and it's not in the deck
                END AS result
            FROM user_cards uc LEFT JOIN deck d ON uc.username = d.username AND uc.card_id = @CardToTrade
            WHERE uc.username = @Username;";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@CardToTrade", tradingDeal.CardToTrade);

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in CheckCardIfUserNotOwnOrInDeckRepo: {ex.Message}");
                return false;
            }
        }

        public bool CheckDealExist(TradingDeal tradingDeal, string username)
        {
            if (tradingDeal == null)
            {
                return false;
            }

            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
            SELECT EXISTS (
                SELECT 1
                FROM trading_deals td
                WHERE td.card_id = @CardId
                AND EXISTS (
                    SELECT 1
                    FROM user_cards uc
                    WHERE uc.card_id = td.card_id
                    AND uc.username = @Username
                )
            );";
                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@CardId", tradingDeal.CardToTrade);

                        return (bool)command.ExecuteScalar(); // True if the deal exists, false otherwise
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in CheckDealExist: {ex.Message}");
                return false;
            }
        }

        public void CreateTrading(TradingDeal tradingDeal)
        {
            if (tradingDeal == null)
            {
                throw new ArgumentException("Trading deal cannot be null.");
            }

            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
            INSERT INTO trading_deals (card_id, type, minimum_damage, trading_id)
            VALUES (@CardId, @Type, @MinimumDamage, @TradingId);";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@CardId", tradingDeal.CardToTrade);
                        command.Parameters.AddWithValue("@Type", tradingDeal.Type);
                        command.Parameters.AddWithValue("@MinimumDamage", tradingDeal.MinimumDamage);
                        command.Parameters.AddWithValue("@TradingId", tradingDeal.ID);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in CreateTrading: {ex.Message}");
                throw; // Consider rethrowing the exception or handling it according to your error policy
            }
        }

        public bool UserHasDeal(Guid tradingId, string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    // Query to check if a user has a deal based on trading_id
                    var query = @"
                SELECT EXISTS (
                    SELECT 1
                    FROM trading_deals td
                    INNER JOIN user_cards uc ON td.card_id = uc.card_id
                    WHERE uc.username = @Username AND td.trading_id = @TradingId
                );";
                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@TradingId", tradingId);

                        // Execute the command and return true if the deal exists, false otherwise
                        return (bool)command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in UserHasDeal: {ex.Message}");
                return false;
            }
        }

        public bool FindDeal(Guid tradingId)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
            SELECT EXISTS (
                SELECT 1
                FROM trading_deals
                WHERE trading_id = @TradingId
            );";
                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@TradingId", tradingId);

                        return (bool)command.ExecuteScalar(); // True if the trading deal exists
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in FindDeal: {ex.Message}");
                return false;
            }
        }

        public void DeleteTrading(Guid tradingId)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = "DELETE FROM trading_deals WHERE trading_id = @TradingId;";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@TradingId", tradingId);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in DeleteTradingRepo: {ex.Message}");
            }
        }

        public bool IfCardIsGoodEnoughOrNotYourOwnCard(Guid tradingId, string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
            SELECT COUNT(*)
            FROM trading_deals td
            JOIN cards c ON td.card_id = c.card_id
            JOIN user_cards uc ON c.card_id = uc.card_id
            WHERE td.trading_id = @TradingId 
              AND (uc.username != @Username OR (td.type = 'monster' AND td.minimum_damage > 10));";
                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@TradingId", tradingId);
                        command.Parameters.AddWithValue("@Username", username);

                        var result = (long)command.ExecuteScalar();
                        return result > 0; // True if the card meets any of the criteria
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in IfCardIsGoodEnoughServiceAndNotYourOwnCardRepo: {ex.Message}");
                return false;
            }
        }

        public void DoTrading(Guid tradingIdHas, Guid tradingIdWantToTrade, Guid idToDelete)
        {
            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Get the package IDs for each card
                        Guid packageIdHas = GetPackageIdByCardId(tradingIdHas);
                        Guid packageIdWants = GetPackageIdByCardId(tradingIdWantToTrade);

                        // Update the package IDs of the cards
                        UpdateCardPackage(tradingIdHas, packageIdWants);
                        UpdateCardPackage(tradingIdWantToTrade, packageIdHas);

                        DeleteTrading(idToDelete);

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        // Rollback the transaction in case of an error
                        transaction.Rollback();

                        // Log the exception
                        Console.WriteLine($"Error in DoTradingRepo: {ex.Message}");
                    }
                }
            }
        }

        public bool UserTryToTradeToHimSelf(Guid tradingId, string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
                SELECT COUNT(*)
                FROM trading_deals td
                JOIN cards c ON td.card_id = c.card_id
                JOIN user_cards uc ON uc.card_id = td.card_id
                WHERE td.trading_id = @TradingId AND uc.username = @Username;";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@TradingId", tradingId);
                        command.Parameters.AddWithValue("@Username", username);

                        var result = (long)command.ExecuteScalar();
                        return result > 0; // True if the user is trying to trade with themselves
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in UserTryToTradeToHimselfRepo: {ex.Message}");
                return false; // Or handle the exception as per your error policy
            }
        }

        public void UpdateCardPackage(Guid cardId, Guid newCardId)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    // Step 1: Find the package that currently contains the cardId
                    Guid currentPackageId = GetPackageIdByCardId(cardId);

                    // Step 2: Find the column name that contains the cardId
                    string columnName = FindColumnNameForCardId(connection, currentPackageId, cardId);

                    // Step 3: Update the package to set the newCardId in the column found
                    UpdatePackageWithNewCardId(connection, currentPackageId, columnName, newCardId);
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in UpdateCardPackage: {ex.Message}");
                throw; // Rethrow the exception after logging
            }
        }

        private string FindColumnNameForCardId(NpgsqlConnection connection, Guid packageId, Guid cardId)
        {
            var query = @"
    SELECT 
        CASE 
            WHEN card_id1 = @CardId THEN 'card_id1'
            WHEN card_id2 = @CardId THEN 'card_id2'
            WHEN card_id3 = @CardId THEN 'card_id3'
            WHEN card_id4 = @CardId THEN 'card_id4'
            WHEN card_id5 = @CardId THEN 'card_id5'
        END AS column_name
    FROM packages
    WHERE package_id = @PackageId;";

            using (var command = new NpgsqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@CardId", cardId);
                command.Parameters.AddWithValue("@PackageId", packageId);

                var result = command.ExecuteScalar();
                if (result != null)
                {
                    return result.ToString();
                }

                throw new Exception("Card not found in any package column.");
            }
        }

        private void UpdatePackageWithNewCardId(NpgsqlConnection connection, Guid packageId, string columnName,
            Guid newCardId)
        {
            var query = $@"
    UPDATE packages
    SET {columnName} = @NewCardId
    WHERE package_id = @PackageId;";

            using (var command = new NpgsqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@NewCardId", newCardId);
                command.Parameters.AddWithValue("@PackageId", packageId);

                command.ExecuteNonQuery();
            }
        }

        public Guid FindTheCardOfTradingId(Guid tradingId)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    var query = @"
            SELECT card_id
            FROM trading_deals
            WHERE trading_id = @TradingId;";
                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@TradingId", tradingId);

                        var result = command.ExecuteScalar();
                        if (result != null && Guid.TryParse(result.ToString(), out Guid cardId))
                        {
                            return cardId; // Return the card ID associated with the trading ID
                        }

                        return Guid.Empty; // Return Guid.Empty if no card ID is found
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in FindTheCardOfTradingId: {ex.Message}");
                return Guid.Empty;
            }
        }

        public Guid GetPackageIdByCardId(Guid cardId)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    var query = @"
            SELECT package_id
            FROM packages
            WHERE @CardId IN (card_id1, card_id2, card_id3, card_id4, card_id5);";

                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@CardId", cardId);

                        var result = command.ExecuteScalar();
                        if (result != null && Guid.TryParse(result.ToString(), out Guid packageId))
                        {
                            return packageId; // Return the package ID if found
                        }

                        return Guid.Empty; // Return Guid.Empty if no package ID is found
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception (log it, etc.)
                Console.WriteLine($"Error in GetPackageIdByCardId: {ex.Message}");
                return Guid.Empty; // Return Guid.Empty or handle the exception as per your error policy
            }
        }
    }
}