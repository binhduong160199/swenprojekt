using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Interfaces;
using Npgsql;

namespace MonsterTradingCardsGame.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly DatabaseConnection _dbConnection;

        public CardRepository(DatabaseConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public List<CardDTO> GetCardsByUsername(string username)
        {
            var cards = new List<CardDTO>();
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                var query = @"
                SELECT c.card_id, c.name, c.damage 
                FROM cards c
                INNER JOIN user_cards uc ON c.card_id = uc.card_id
                WHERE uc.username = @Username";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new CardDTO
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Damage = reader.GetDouble(2),
                            };
                            cards.Add(card);
                        }
                    }
                }
            }
            return cards;
        }

        public List<CardDTO> GetCardsByUsernameForBattle(string username)
        {
            var cards = new List<CardDTO>();
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                var query = @"
                SELECT c.card_id, c.name, c.damage 
                FROM cards c
                INNER JOIN deck d ON c.card_id = d.card_id
                WHERE d.username = @Username";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new CardDTO
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Damage = reader.GetDouble(2),
                            };
                            cards.Add(card);
                        }
                    }
                }
            }

            return cards;
        }
    }
}