using NUnit.Framework;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.Services;
using MonsterTradingCardsGame.Models;
using Npgsql;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class ScoreboardTest
    {
        private DatabaseConnection _dbConnection;
        private UserService _userService;
        private UserRepository _userRepository;
        private CardService _cardService;
        private CardRepository _cardRepository;
        private DeckRepository _deckRepository;

        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
            _cardRepository = new CardRepository(_dbConnection);
            _cardService = new CardService(_cardRepository);
            _userRepository = new UserRepository(_dbConnection);
            _userService = new UserService(_userRepository);
        }

        [Test]
        public void GetUserScoreboardByUserElo_ShouldReturnSortedByElo()
        {
            // Arrange

            // Act
            var scoreboard = GetUserScoreboardByUserElo();

            // Assert
            Assert.That(scoreboard, Is.Not.Null, "The scoreboard list should not be null.");
            Assert.That(scoreboard, Is.Not.Empty, "The scoreboard list should not be empty.");

            // Verify that the list is sorted by Elo in descending order
            for (int i = 1; i < scoreboard.Count; i++)
            {
                Assert.That(scoreboard[i].Elo, Is.LessThanOrEqualTo(scoreboard[i - 1].Elo),
                    $"The list should be sorted by Elo, but found {scoreboard[i].Elo} after {scoreboard[i - 1].Elo}.");
            }
        }

        public List<UserStats> GetUserScoreboardByUserElo()
        {
            List<UserStats> userStatsList = new List<UserStats>();

            using (var connection = _dbConnection.GetConnection())
            {
                connection.Open();
                using (var command =
                       new NpgsqlCommand("SELECT username, elo, wins, losses FROM user_stats", connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read()) // Iterate over all rows in the result set
                        {
                            var userStats = new UserStats
                            {
                                Name = reader["username"].ToString(),
                                Elo = Convert.ToInt32(reader["elo"]),
                                Wins = Convert.ToInt32(reader["wins"]),
                                Losses = Convert.ToInt32(reader["losses"])
                            };
                            userStatsList.Add(userStats);
                        }
                    }
                }
            }

            return userStatsList; // Return the list of UserStats
        }
    }
}