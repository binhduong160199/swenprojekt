using NUnit.Framework;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.Services;
using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Interfaces;
using Moq;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class UserTest
    {
        private DatabaseConnection _dbConnection;
        private UserService _userService;
        private UserRepository _userRepository;
        private Mock<IUserRepository> _mockUserRepository;
        private Mock<IUserService> _mockUserService;

        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
            _userRepository = new UserRepository(_dbConnection);
            _userService = new UserService(_userRepository);
            _mockUserRepository = new Mock<IUserRepository>();
            
        }

        [Test]
        public void AuthenticateUser_ShouldReturnFalse_WhenCredentialsAreInvalid()
        {
            // Arrange
            var username = "validUser";
            var password = "invalidPassword";
            var storedHash = "someHash";

            _mockUserRepository.Setup(repo => repo.GetPasswordHashByUsername(username)).Returns(storedHash);

            // Act
            var result = _userService.AuthenticateUser(username, password);

            // Assert
            Assert.That(result, Is.False, "Should return false for invalid credentials.");
        }
        
        [Test]
        public void DoesUserExist_ShouldReturnTrue_WhenUserExists()
        {
            // Arrange
            var username = "kienboec";

            // Act
            var result = _userRepository.DoesUserExist(username);

            // Assert
            Assert.That(result, Is.True, "Should return true when the user exists.");
        }

        [Test]
        public void DoesUserExist_ShouldReturnFalse_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonExistingUser";

            // Act
            var result = _userRepository.DoesUserExist(username);

            // Assert
            Assert.That(result, Is.False, "Should return false when the user does not exist.");
        }
    }
}