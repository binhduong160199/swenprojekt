using NUnit.Framework;
using MonsterTradingCardsGame.Database;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class DatabaseConnectionTest

    {
        private DatabaseConnection _dbConnection;

        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
        }

        [Test]
        public void TestDatabaseConnection_ShouldReturnTrueWhenConnectionSucceeds()
        {
            // Act
            var result = _dbConnection.TestDatabaseConnection();

            // Assert
            Assert.That(result, Is.True, "The method should return true when the database connection is successful.");
        }
    }
}