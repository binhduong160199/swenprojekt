using NUnit.Framework;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.Services;
using MonsterTradingCardsGame.Models;
using Npgsql;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class DeckTest
    {
        private DatabaseConnection _dbConnection;
        private UserService _userService;
        private UserRepository _userRepository;
        private CardService _cardService;
        private CardRepository _cardRepository;
        private DeckRepository _deckRepository;

        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
            _cardRepository = new CardRepository(_dbConnection);
            _cardService = new CardService(_cardRepository);
            _userRepository = new UserRepository(_dbConnection);
            _userService = new UserService(_userRepository);
        }

        [Test]
        public void GetDeckFromUser()
        {
            var testUsername = "kienboec";
            var deck = GetDeckByUsername(testUsername);
            Assert.That(deck, Is.Not.Null, "The deck should not be null.");
            Assert.That(deck, Is.Not.Empty, "The deck should not be empty for the user with a predefined deck.");
        }
        
        [Test]
        public void DoesDeckConfigurationExist_ShouldReturnTrueWhenConfigurationExists()
        {
            // Arrange
            var existingCardIds = new List<string> 
            { 
                "845f0dc7-37d0-426e-994e-43fc3ac83c08", 
                "99f8f8dc-e25e-4a95-aa2c-782823f36e2a",
                "e85e3976-7c86-4d06-9a80-641c2019a79f",
                "171f6076-4eb5-4a7d-b3f2-2d650cc3d237"
            };

            // Act
            var result1 = DoesDeckConfigurationExist(existingCardIds);

            // Assert
            Assert.That(result1, Is.True, "The method should return true when all card IDs exist in the deck.");
        }

        [Test]
        public void DoesDeckConfigurationExist_ShouldReturnFalseWhenAnyCardIsNotInDeck()
        {
            // Arrange
            var cardIdsWithOneNonexistent = new List<string> 
            { 
                "845f0dc7-37d0-426e-994e-43fc3ac83c08", 
                "99f8f8dc-e25e-4a95-aa2c-782823f36e2a",
                "e85e3976-7c86-4d06-9a80-641c2019a79f",
                "171f6076-4eb5-4a7d-b3f2-2d220cc3d237"
            };

            // Act
            var result2 = DoesDeckConfigurationExist(cardIdsWithOneNonexistent);

            // Assert
            Assert.That(result2, Is.False, "The method should return false when any card ID does not exist in the deck.");
        }
        
        
        

        public List<CardDTO> GetDeckByUsername(string username)
        {
            var cards = new List<CardDTO>();
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
            SELECT c.card_id, c.name, c.damage
            FROM cards c
            INNER JOIN deck d ON c.card_id = d.card_id
            WHERE d.username = @Username";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new CardDTO
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Damage = reader.GetDouble(2),
                            };
                            cards.Add(card);
                        }
                    }
                }
            }

            return cards;
        }
        public bool DoesDeckConfigurationExist(List<string> cardIdList)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();

                    foreach (var cardId in cardIdList)
                    {
                        var query = @"
                    SELECT COUNT(*)
                    FROM deck
                    WHERE card_id = @CardId;";

                        using (var command = new NpgsqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@CardId", Guid.Parse(cardId));

                            var count = (long)command.ExecuteScalar();
                            if (count == 0)
                            {
                                // If any card is not found in the deck table, the configuration does not exist
                                return false;
                            }
                        }
                    }
                    // All cards in the list exist in the deck table
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in DoesDeckConfigurationExist: {ex.Message}");
                return false;
            }
        }
    }
}