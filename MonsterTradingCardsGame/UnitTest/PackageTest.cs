using NUnit.Framework;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.Services;
using MonsterTradingCardsGame.Models;
using MonsterTradingCardsGame.Interfaces;
using Npgsql;
using Moq;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class PackageTest
    {
        private DatabaseConnection _dbConnection;
        private UserService _userService;
        private UserRepository _userRepository;
        private CardService _cardService;
        private CardRepository _cardRepository;
        private DeckRepository _deckRepository;
        private Mock<IPackageRepository> _mockPackageRepository;


        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
            _cardRepository = new CardRepository(_dbConnection);
            _cardService = new CardService(_cardRepository);
            _userRepository = new UserRepository(_dbConnection);
            _userService = new UserService(_userRepository);
            _mockPackageRepository = new Mock<IPackageRepository>();
        }

        [Test]
        public void GetCardsFromLastPurchasedPackage_ShouldReturnLastFiveCards()
        {
            // Arrange
            var testUsername = "kienboec";

            // Act
            var lastPurchasedCards = GetCardsFromLastPurchasedPackage(testUsername);

            // Assert
            Assert.That(lastPurchasedCards, Is.Not.Null, "The list of cards should not be null.");
            Assert.That(lastPurchasedCards, Has.Count.EqualTo(5),
                "The list should contain exactly 5 cards, representing the last purchased package.");
        }

        [Test]
        public void AreUnacquiredCardsAvailable_ShouldReturnTrueWhenCardsAreAvailable()
        {
            // Arrange

            // Act
            var result = AreUnacquiredCardsAvailable();

            // Assert
            Assert.That(result, Is.True, "The method should return true when unacquired cards are available.");
        }

        [Test]
        public void GetAvailablePackage_ShouldReturnPackage()
        {
            // Arrange

            // Act
            var package = GetAvailablePackage();

            // Assert
            Assert.That(package, Is.Not.Null, "Should return an available package.");
            // Further asserts can be made to check the contents of the package
        }

        [Test]
        public void IsPackageAvailable_ShouldReturnTrue_WhenUserHasNotReachedMaxPackages()
        {
            // Arrange
            var mockPackageRepo = new Mock<IPackageRepository>();
            var mockUserRepo = new Mock<IUserRepository>();
            var packageService = new PackageService(mockPackageRepo.Object, mockUserRepo.Object);
            var userId = "testUser";

            mockPackageRepo.Setup(repo => repo.AreUnacquiredCardsAvailable()).Returns(true);

            // Act
            var result = packageService.IsPackageAvailable(userId);

            // Assert
            Assert.That(result, Is.True, "Expected true but was false.");
        }


        public List<CardDTO> GetCardsFromLastPurchasedPackage(string username)
        {
            var cards = new List<CardDTO>();
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();

                var query = @"
            SELECT c.card_id, c.name, c.damage 
            FROM cards c
            INNER JOIN user_cards uc ON c.card_id = uc.card_id
            WHERE uc.username = @Username
            ORDER BY uc.acquired_at DESC
            LIMIT 5";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var card = new CardDTO
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Damage = reader.GetDouble(2),
                            };
                            cards.Add(card);
                        }
                    }
                }
            }

            return cards;
        }

        public bool AreUnacquiredCardsAvailable()
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
SELECT EXISTS (
    SELECT 1 FROM cards c
    WHERE NOT EXISTS (
        SELECT 1 FROM user_cards uc WHERE uc.card_id = c.card_id
    )
) AS available;";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    var result = (bool)cmd.ExecuteScalar();
                    return result; // Returns true if there are unacquired cards, false otherwise
                }
            }
        }

        public Package GetAvailablePackage()
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
SELECT p.*
FROM packages p
WHERE NOT EXISTS (
    SELECT 1 FROM user_cards uc WHERE uc.card_id IN (p.card_id1, p.card_id2, p.card_id3, p.card_id4, p.card_id5)
)
ORDER BY RANDOM()
LIMIT 1;
";
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var packageId = reader.GetGuid(0);
                            var cards = new List<Card>(); // List to hold cards

                            for (int i = 1; i <= 5; i++)
                            {
                                if (!reader.IsDBNull(i))
                                {
                                    var cardId = reader.GetGuid(i);
                                    var card = GetCardById(cardId);
                                    if (card != null)
                                    {
                                        cards.Add(card);
                                    }
                                }
                            }

                            // Return a package with the retrieved ID and list of cards
                            return new Package(packageId, cards);
                        }
                    }
                }
            }

            return null; // Return null if no package is found
        }

        public Card GetCardById(Guid cardId)
        {
            using (var conn = _dbConnection.GetConnection())
            {
                conn.Open();
                var query = @"
            SELECT card_id, name, damage
            FROM cards
            WHERE card_id = @CardId";

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    // Explicitly specify the type of the parameter
                    cmd.Parameters.Add("@CardId", NpgsqlTypes.NpgsqlDbType.Uuid).Value = cardId;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var id = reader.GetGuid(0);
                            var name = reader.GetString(1);
                            var damage = reader.GetDouble(2);
                            var cardType = DetermineCardType(name);

                            // Create the appropriate card based on the card type
                            switch (cardType)
                            {
                                case CardTypeEnum.Spell:
                                    return new SpellCard(id, name, damage);

                                case CardTypeEnum.Monster:
                                default:
                                    return new MonsterCard(id, name, damage);
                            }
                        }
                    }
                }
            }

            return null; // Return null if no card is found
        }

        private CardTypeEnum DetermineCardType(string name)
        {
            // Check if the name contains "Spell" to classify it as a Spell card
            if (name.Contains("Spell", StringComparison.OrdinalIgnoreCase))
            {
                return CardTypeEnum.Spell;
            }
            // Otherwise, classify it as a Monster card by default
            else
            {
                return CardTypeEnum.Monster;
            }
        }
    }
}