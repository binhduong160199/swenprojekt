using NUnit.Framework;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.Services;
using MonsterTradingCardsGame.Models;
using Npgsql;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class UserStatsTest
    {
        private DatabaseConnection _dbConnection;
        private UserService _userService;
        private UserRepository _userRepository;
        private CardService _cardService;
        private CardRepository _cardRepository;
        private DeckRepository _deckRepository;

        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
            _cardRepository = new CardRepository(_dbConnection);
            _cardService = new CardService(_cardRepository);
            _userRepository = new UserRepository(_dbConnection);
            _userService = new UserService(_userRepository);
        }

        [Test]
        public void GetUserStatsByUsername_ShouldReturnUserStats()
        {
            // Arrange
            var testUsername = "kienboec";

            // Act
            var userStats = GetUserStatsByUsername(testUsername);

            // Assert
            Assert.That(userStats, Is.Not.Null, "UserStats should not be null for an existing user.");
            Assert.That(userStats.Name, Is.EqualTo(testUsername), "The username should match the requested username.");


            Assert.That(userStats.Elo, Is.EqualTo(100), "Elo should match the expected value.");
            Assert.That(userStats.Wins, Is.EqualTo(0), "Wins should match the expected value.");
            Assert.That(userStats.Losses, Is.EqualTo(0), "Losses should match the expected value.");
        }

        [Test]
        public void GetUserStatsByUsername_ShouldReturnNullForNonexistentUser()
        {
            // Arrange
            var nonExistentUsername = "somebody";

            // Act
            var userStats = GetUserStatsByUsername(nonExistentUsername);

            // Assert
            Assert.That(userStats, Is.Null, "UserStats should be null for a non-existent user.");
        }

        public UserStats GetUserStatsByUsername(string username)
        {
            try
            {
                using (var connection = _dbConnection.GetConnection())
                {
                    connection.Open();
                    using (var command =
                           new NpgsqlCommand(
                               "SELECT username, elo, wins, losses FROM user_stats WHERE username = @Username",
                               connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return new UserStats
                                {
                                    Name = reader["username"].ToString(),
                                    Elo = Convert.ToInt32(reader["elo"]),
                                    Wins = Convert.ToInt32(reader["wins"]),
                                    Losses = Convert.ToInt32(reader["losses"])
                                };
                            }
                        }
                    }
                }

                return null; // Return null if user not found
            }
            catch (NpgsqlException ex)
            {
                // Log exception and handle it appropriately
                Console.WriteLine($"Error in GetUserStatsByUsername: {ex.Message}");
                return null;
            }
        }
    }
}