using NUnit.Framework;
using MonsterTradingCardsGame.Database;
using MonsterTradingCardsGame.Repositories;
using MonsterTradingCardsGame.Models;

namespace MonsterTradingCardsGame.UnitTest
{
    [TestFixture]
    public class TradingRepositoryIntegrationTests
    {
        private DatabaseConnection _dbConnection;
        private TradingRepository _tradingRepository;

        [SetUp]
        public void Setup()
        {
            _dbConnection = new DatabaseConnection();
            _tradingRepository = new TradingRepository(_dbConnection);
        }
        
        [Test]
        public void GetTradingDeal_ShouldReturnEmptyList_WhenCalledWithInvalidUser()
        {
            // Arrange
            var username = "nonExistingUser";

            // Act
            var tradingDeals = _tradingRepository.GetTradingDeal(username);

            // Assert
            Assert.That(tradingDeals, Is.Empty, "Should return an empty list for a non-existing user.");
        }

        [Test]
        public void CheckDealExist_ShouldReturnFalse_WhenDealDoesNotExist()
        {
            // Arrange
            var username = "kienboec"; 
            var tradingDeal = new TradingDeal
            {
                CardToTrade = new Guid("99f8f8dc-e25e-4a95-aa2c-782823f36e2a") 
            };

            // Act
            var result = _tradingRepository.CheckDealExist(tradingDeal, username);

            // Assert
            Assert.That(result, Is.False, "Should return false when the deal does not exist.");
        }

        [Test]
        public void CheckDealExist_ShouldReturnFalse_WhenTradingDealIsNull()
        {
            // Arrange
            var username = "kienboec"; 

            // Act
            var result = _tradingRepository.CheckDealExist(null, username);

            // Assert
            Assert.That(result, Is.False, "Should return false when the trading deal is null.");
        }
    }
}